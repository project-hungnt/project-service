package com.hungnt.firebase;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.hungnt.config.Constants;
import com.hungnt.domain.FileUpload;
import com.hungnt.utils.FileUtil;
import com.hungnt.web.rest.errors.FormValidateException;
import com.hungnt.web.rest.vm.fileUpload.FileCreateVM;
import com.hungnt.web.rest.vm.fileUpload.FileRequestWrap;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
public class FirebaseInitializeApp {

    public FirebaseInitializeApp() {
        try {
            FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(getCredentials())
                .setStorageBucket(Constants.STORAGE_BUCKET)
                .build();
            if (FirebaseApp.getApps().isEmpty()) { //<--- check with this line
                FirebaseApp.initializeApp(options);
            }
        } catch (IOException e) {
            throw new FormValidateException("file_name", e.getMessage());
        }

    }

    public List<FileUpload> uploadFile(FileRequestWrap fileRequestWrap) throws IOException {
        List<FileUpload> fileUploads = new ArrayList<>();
        Storage storage = StorageOptions
            .newBuilder()
            .setCredentials(getCredentials())
            .build()
            .getService();
        fileRequestWrap.getFileRequests().forEach(file -> {

            BigDecimal fileSize = FileUtil.calculateFileSizeInMB(file.getBase64());
            if (fileSize.compareTo(BigDecimal.valueOf(5)) > 0) {
                throw new FormValidateException("size", "must smaller 5M");
            }
            byte[] bytes = FileUtil.base64ToByte(file.getBase64());
            if (StringUtils.isBlank(file.getFileName())) {
                throw new FormValidateException("file_name", "not blank");
            }
            String extension = FileUtil.getExtension(file.getFileName());
            if (!FileUtil.FILE_EXTENSION_JPG.equalsIgnoreCase(extension)
                && !FileUtil.FILE_EXTENSION_PDF.equalsIgnoreCase(extension)
                && !FileUtil.FILE_EXTENSION_PNG.equalsIgnoreCase(extension)
                && !FileUtil.FILE_EXTENSION_RAR.equalsIgnoreCase(extension)) {
                throw new FormValidateException("extension", "error format");
            }
            String fileName = FileUtil.generateFileName(null, file.getFileName(), null, extension);
            Map<String, String> map = new HashMap<>();
            map.put("firebaseStorageDownloadTokens", fileName);
            BlobId blobId = BlobId.of(Constants.STORAGE_BUCKET, fileName);
            BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setMetadata(map).setContentType(extension).build();
            Blob blob = storage.create(blobInfo,bytes);
            String tokenImage = blob.getMetadata().get("firebaseStorageDownloadTokens");
            FileUpload fileUpload = FileUpload.builder()
                .provider(fileRequestWrap.getProvider().toUpperCase())
                .fileName(fileName)
                .fileUrl(generateUrl(tokenImage))
                .fileExtension(extension)
                .referenceId(fileRequestWrap.getReferenceId())
                .build();
            fileUploads.add(fileUpload);
        });
        return fileUploads;
    }
    public FileUpload uploadOnFile(FileCreateVM file) throws IOException {
        Storage storage = StorageOptions
            .newBuilder()
            .setCredentials(getCredentials())
            .build()
            .getService();
        BigDecimal fileSize = FileUtil.calculateFileSizeInMB(file.getBase64());
        if (fileSize.compareTo(BigDecimal.valueOf(5)) > 0) {
            throw new FormValidateException("size", "must smaller 5M");
        }
        byte[] bytes = FileUtil.base64ToByte(file.getBase64());
        if (StringUtils.isBlank(file.getFileName())) {
            throw new FormValidateException("file_name", "not blank");
        }
        String extension = FileUtil.getExtension(file.getFileName());
        if (!FileUtil.FILE_EXTENSION_JPG.equalsIgnoreCase(extension)
            && !FileUtil.FILE_EXTENSION_PDF.equalsIgnoreCase(extension)
            && !FileUtil.FILE_EXTENSION_PNG.equalsIgnoreCase(extension)
            && !FileUtil.FILE_EXTENSION_RAR.equalsIgnoreCase(extension)) {
            throw new FormValidateException("extension", "error format");
        }
        String fileName = FileUtil.generateFileName(null, file.getFileName(), null, extension);
        Map<String, String> map = new HashMap<>();
        map.put("firebaseStorageDownloadTokens", fileName);
        BlobId blobId = BlobId.of(Constants.STORAGE_BUCKET, fileName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setMetadata(map).setContentType(extension).build();
        Blob blob = storage.create(blobInfo,bytes);
        String tokenImage = blob.getMetadata().get("firebaseStorageDownloadTokens");
        FileUpload fileUpload = FileUpload.builder()
            .provider(file.getProvider().toUpperCase())
            .fileName(fileName)
            .fileUrl(generateUrl(tokenImage))
            .fileExtension(extension)
            .referenceId(file.getReferentId())
            .build();
        return fileUpload;
    }

    public String generateUrl(String token){
        return Constants.STORAGE_URL+token+"?alt=media&token="+token;
    }
    public GoogleCredentials getCredentials() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream inputStream = classLoader.getResourceAsStream("service_account.json");
        assert inputStream != null;
        return GoogleCredentials.fromStream(inputStream);
    }
}
