package com.hungnt.clients;

import com.hungnt.config.Constants;
import com.hungnt.config.FeignClientConfig;
import com.hungnt.web.rest.vm.qr.ListBankVM;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "VietQrClient",configuration = FeignClientConfig.class, url = Constants.VIET_QR)
public interface VietQrClient {
    @GetMapping("/v2/banks")
    ListBankVM getBanks();
}
