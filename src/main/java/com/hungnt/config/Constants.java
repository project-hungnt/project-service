package com.hungnt.config;

/**
 * Application constants.
 */
public final class Constants {

    // Regex for acceptable logins
    public static final String LOGIN_REGEX = "^(?>[a-zA-Z0-9!$&*+=?^_`{|}~.-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*)|(?>[_.@A-Za-z0-9-]+)$";
    public static final String DATE_PATTERN = "^(29-02-(2000|2400|2800|(19|2[0-9](0[48]|[2468][048]|[13579][26]))))$"
        + "|^(((0[1-9]|1[0-9]|2[0-8])-02-(19|2[0-9])[0-9]{2}))$"
        + "|^(((0[1-9]|[12][0-9]|3[01])-(0[13578]|10|12)-(19|2[0-9])[0-9]{2}))$"
        + "|^(((0[1-9]|[12][0-9]|30)-(0[469]|11)-(19|2[0-9])[0-9]{2}))$";
    public static final String SYSTEM_ACCOUNT = "system";
    public static final String DEFAULT_LANGUAGE = "en";
    public static final String ANONYMOUS_USER = "anonymoususer";
    public static final String VIET_QR="https://api.vietqr.io";
    public static final String STORAGE_BUCKET="upload-file-service-1b20f.appspot.com";
    public static final String STORAGE_URL ="https://firebasestorage.googleapis.com/v0/b/upload-file-service-1b20f.appspot.com/o/";
    public static final String REMOVE_CATEGORY_AND_PRODUCT="REMOVE_CATEGORY_AND_PRODUCT";
    public static final String STATUS_ACTIVE="ACTIVE";
    public static final String STATUS_STOP_SALE="STOP_SALE";
    public static final String STATUS_DELETED="DELETED";
    public static final String DISCOUNT_ALL_LOCATION ="ALL_LOCATION";
    public static final String DISCOUNT_SOME_LOCATION ="SOME_LOCATION";
    public static final String PRODUCT_IMAGE ="PRODUCT_IMAGE";
    public static final String PRODUCT_DETAIL_IMAGE ="PRODUCT_DETAIL_IMAGE";
    public static final String ORDER_PAID="PAID";
    public static final String ORDER_WAITING="WAITING";
    public static final String ORDER_FAIL="FAIL";
    public static final String ORDER_CANCEL="CANCEL";
    public static final String USER_ACTIVE="ACTIVE";
    public static final String USER_STOP="STOP";

    private Constants() {
    }
}
