package com.hungnt.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.Data;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.stereotype.Repository;

@Data
@Repository
@Table(name = "cities")
@Entity
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CityDomain implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long id;

    @NotBlank
    @Size(max = 255)
    @Column(name = "city_name", nullable = false)
    private String cityName;

    @NotBlank
    @Size(max = 255)
    @Column(name = "city_code", nullable = false)
    private String cityCode;

    @NotBlank
    @Size(max = 255)
    @Column(name = "country_name", nullable = false)
    private String countryName;

    @NotBlank
    @Size(max = 255)
    @Column(name = "country_code", nullable = false)
    private String countryCode;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CityDomain)) {
            return false;
        }
        return id != null && id.equals(((CityDomain) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "City{" +
            "id=" + id +
            ", cityName='" + cityName + '\'' +
            ", cityCode='" + cityCode + '\'' +
            ", countryName='" + countryName + '\'' +
            ", countryCode='" + countryCode + '\'' +
            '}';
    }
}
