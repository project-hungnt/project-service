package com.hungnt.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Builder
@Entity
@Table(name = "file_uploads")
@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class FileUpload extends BaseDomain {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long id;


    @NotBlank
    @Column(name = "file_url", nullable = false)
    private String fileUrl;

    @NotBlank
    @Size(max = 255)
    @Column(name = "file_name", nullable = false)
    private String fileName;

    @Size(max = 5)
    @Column(name = "file_extension", length = 5, nullable = false)
    private String fileExtension;

    @Column(name = "deleted", nullable = false)
    private boolean deleted;

    @Column(name = "reference_id", nullable = false, updatable = false)
    private Long referenceId;

    @Column(name = "provider", length = 50, nullable = false)
    private String provider;
}
