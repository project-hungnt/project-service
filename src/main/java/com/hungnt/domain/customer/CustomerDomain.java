package com.hungnt.domain.customer;

import com.hungnt.config.Constants;
import com.hungnt.domain.BaseDomain;
import com.hungnt.security.SecurityUtils;
import com.hungnt.utils.TextUtils;
import com.hungnt.web.rest.vm.customer.CustomerRequestVM;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "customers" ,uniqueConstraints = {@UniqueConstraint(columnNames = {"email", "phone"})})
public class    CustomerDomain extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",nullable = false, updatable = false)
    private Long id;

    @Column(name = "code")
    private String code;

    @Column(name = "phone")
    private String phone;

    @Column(name = "gender")
    private String gender= "MALE";

    @Column(name = "name")
    private String name;

    @Column(name = "birth")
    @Size(max = 20)
    private String birth;

    @Column(name = "email")
    private String email;

    @Column(name = "city_code")
    private String cityCode;

    @Column(name = "ward_code")
    private String wardCode;

    @Column(name = "district_code")
    private String districtCode;

    @Column(name = "city_name")
    private String cityName;

    @Column(name = "ward_name")
    private String wardName;

    @Column(name = "district_name")
    private String districtName;

    @Column(name = "address")
    private String address;

    @Column(name = "total_orders")
    private Integer totalOrders=0;

    @Column(name = "total_sales")
    private BigDecimal totalSales= BigDecimal.valueOf(0);

    public CustomerDomain update(CustomerRequestVM customerRequestVM){
        setName(customerRequestVM.getName());
        this.setAddress(customerRequestVM.getAddress());
        this.setPhone(customerRequestVM.getPhone());
        this.setBirth(customerRequestVM.getBirth());
        this.setGender(customerRequestVM.getGender());
        this.setWardCode(customerRequestVM.getWardName());
        this.setCityCode(customerRequestVM.getCityName());
        this.setDistrictCode(customerRequestVM.getDistrictName());
        this.setWardName(customerRequestVM.getWardName());
        this.setCityName(customerRequestVM.getCityName());
        this.setDistrictName(customerRequestVM.getDistrictName());
        this.setEmail(customerRequestVM.getEmail());
        this.setLastModifiedBy(SecurityUtils.getCurrentUserLogin().orElse(Constants.SYSTEM_ACCOUNT));
        this.setLastModifiedDate(Instant.now());
        return this;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = TextUtils.generatePropertyCode(cityCode);
    }

    public void setWardCode(String wardCode) {
        this.wardCode = TextUtils.generatePropertyCode(wardCode);
    }

    public void setDistrictCode(String districtCode) {
        this.districtCode = TextUtils.generatePropertyCode(districtCode);
    }
}
