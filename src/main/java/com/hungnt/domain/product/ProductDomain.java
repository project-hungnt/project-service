package com.hungnt.domain.product;

import com.hungnt.config.Constants;
import com.hungnt.domain.BaseDomain;
import com.hungnt.security.SecurityUtils;
import com.hungnt.web.rest.vm.product.ProductUpdateRequest;
import java.io.Serializable;
import java.time.Instant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Entity
@Table(name = "products")
@RequiredArgsConstructor
public class ProductDomain extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long id;

    @NotBlank
    @Column(name = "product_code",length = 50)
    private String productCode;
    @NotBlank
    @Column(name = "product_name")
    private String productName;

    @Column(name = "category_id",length = 50)
    private Long categoryId;

    @Column(name = "description")
    private String description;

    @Column(name = "location_id",length = 50)
    private Long locationId;

    @Column(name = "default_image")
    private String defaultImage;

    @Column(name ="status")
    private String status= Constants.STATUS_ACTIVE;
    public ProductDomain update(ProductUpdateRequest productUpdateRequest){
        this.setProductName(productUpdateRequest.getProductName());
        this.setDescription(productUpdateRequest.getDescription());
        this.setCategoryId(productUpdateRequest.getCategoryId());
        this.setLastModifiedBy(SecurityUtils.getCurrentUserLogin().orElse(Constants.SYSTEM_ACCOUNT));
        this.setLastModifiedDate(Instant.now());
        return this;
    }
}
