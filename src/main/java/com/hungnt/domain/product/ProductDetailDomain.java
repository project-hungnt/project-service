package com.hungnt.domain.product;

import com.hungnt.config.Constants;
import com.hungnt.domain.BaseDomain;
import com.hungnt.security.SecurityUtils;
import com.hungnt.web.rest.vm.product.ProductDetailUpdateRequest;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Entity
@Table(name = "product_details")
@RequiredArgsConstructor
public class ProductDetailDomain extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long id;

    @Column(name = "product_id")
    @NotNull
    private Long productId;
    @Column(name = "product_code")
    @NotNull
    private String productCode;
    @Column(name = "category_id")
    private Long categoryId;

    @Column(name = "code")
    @NotNull
    private String code;
    //   thông tin thuộc tính
    @Column(name = "size")
    private String size;

    @Column(name = "color")
    private String color;

    //chất liệu
    @Column(name = "material")
    private String material;
    //    giá nhập
    @Column(name = "price")
    @NotNull
    @Min(0)
    private BigDecimal price= BigDecimal.valueOf(0);

    @Column(name = "status")
    @NotBlank
    private String status = Constants.STATUS_ACTIVE;

    @Column(name = "barcode")
    private String barCode;

    //    giá bán
    @Column(name = "price_sell")
    @Min(0)
    private BigDecimal priceSell=BigDecimal.valueOf(0);

    //    giảm giá trên giao diện cho chọn %
    @Column(name = "discount")
    @Min(0)
    private Integer discount;

    @Column(name = "quantity")
    @Min(0)
    private int quantity=0;
    //    có thể bán
    @Column(name = "quantity_sell")
    @Min(0)
    private Integer quantitySell=0;

    @Column(name = "location_id")
    private Long locationId;
//    tồn kho
   @Column(name = "product_name")
    private String productName;

    @Column(name = "default_image")
    private String defaultImage;

    public ProductDetailDomain update(ProductDetailUpdateRequest productDetailUpdateRequest){
        this.setQuantity(productDetailUpdateRequest.getQuantity());
        this.setQuantitySell(productDetailUpdateRequest.getQuantitySell());
        this.setProductName(productDetailUpdateRequest.getProductName());
        this.setPriceSell(productDetailUpdateRequest.getPriceSell());
        this.setPrice(productDetailUpdateRequest.getPrice());
        this.setColor(productDetailUpdateRequest.getColor());
        this.setSize(productDetailUpdateRequest.getSize());
        this.setCategoryId(productDetailUpdateRequest.getCategoryId());
        this.setDiscount(productDetailUpdateRequest.getDiscount());
        this.setLastModifiedBy(SecurityUtils.getCurrentUserLogin().orElse(Constants.SYSTEM_ACCOUNT));
        this.setLastModifiedDate(Instant.now());
        return this;
    }
}
