package com.hungnt.domain;

import com.hungnt.config.Constants;
import com.hungnt.domain.BaseDomain;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.RequiredArgsConstructor;
@Data
@Entity
@Table(name = "inventories")
@RequiredArgsConstructor
public class InventoryDomain extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long id;

    @NotNull
    @Column(name = "product_detail_id")
    private Long productDetailId;

    @Column(name = "quantity")
    @Min(0)
    private int quantity;
    //    có thể bán
    @Column(name = "quantity_sell")
    @Min(0)
    private Integer quantitySell;

    @Column(name = "location_id")
    private Long locationId;

}
