package com.hungnt.domain.purchase;

import com.hungnt.config.Constants;
import com.hungnt.domain.BaseDomain;
import com.hungnt.domain.order.OrderDomain;
import com.hungnt.security.SecurityUtils;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "purchase_orders")
public class PurchaseOrdersDomain extends BaseDomain implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long id;

    @NotNull(message = "accountId is required")
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "status")
    @NotNull(message = "Status is required")
    private String status;

    @Column(name = "total_price")
    @NotNull(message = "totalPrice is required")
    private BigDecimal totalPrice;

    @NotNull
    @Column(name = "supplier_id")
    private Long supplierId;

    @Column(name = "total_quantity")
    @NotNull(message = "Total quantity is required")
    private int totalQuantity;

    @Column(name = "money")
    @NotNull(message = "Money is required")
    private BigDecimal money;

    @Column(name = "note")
    @Size(max = 255)
    private String note;

    @Column(name = "payment_method")
    @NotNull
    private String paymentMethod;
    @Column(name = "code")
    private String code;

    public PurchaseOrdersDomain updateStatus(String status) {
        setStatus(status);
        setLastModifiedBy(SecurityUtils.getCurrentUserLogin().orElse(Constants.SYSTEM_ACCOUNT));
        setLastModifiedDate(Instant.now());
        return this;
    }
}
