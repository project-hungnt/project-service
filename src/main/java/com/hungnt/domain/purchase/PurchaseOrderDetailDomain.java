package com.hungnt.domain.purchase;

import com.hungnt.domain.BaseDomain;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Entity
@Table(name = "purchase_order_details")
@RequiredArgsConstructor
public class PurchaseOrderDetailDomain extends BaseDomain implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)

    private Long id;
    @Column(name = "purchase_order_id")
    private Long purchaseOrderId;

    @Column(name = "quantity")
    private int quantity;

    @Column(name = "price")
    private BigDecimal price;

    @Column(name = "product_detail_id")
    private Long productDetailId;


    @Column(name = "purchase_order_code")
    private String purchaseOrderCode;
}
