package com.hungnt.domain;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "districts")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Data
@RequiredArgsConstructor
public class DistrictDomain implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long id;

    @Size(max = 255)
    @Column(name = "city_code", nullable = false)
    private String cityCode;

    @Size(max = 255)
    @Column(name = "district_name", nullable = false)
    private String districtName;

    @Size(max = 255)
    @Column(name = "district_code", nullable = false, unique = true)
    private String districtCode;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DistrictDomain)) {
            return false;
        }
        return id != null && id.equals(((DistrictDomain) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "District{" +
            "id=" + id +
            ", cityCode='" + cityCode + '\'' +
            ", districtName='" + districtName + '\'' +
            ", districtCode='" + districtCode + '\'' +
            '}';
    }
}
