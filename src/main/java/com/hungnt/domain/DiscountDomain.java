package com.hungnt.domain;

import com.google.firebase.database.annotations.NotNull;
import com.hungnt.config.Constants;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Entity
@Table(name = "discounts")
@RequiredArgsConstructor
public class DiscountDomain extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long id;

    // giảm cho toàn bộ chi nhánh
    @Column(name = "type")
    private String type= Constants.DISCOUNT_ALL_LOCATION;

    @Column(name = "code")
    @NotBlank
    private String code;

    @Column(name = "expiredDate")
    @NotNull
    private Instant expiredDate;

    @Column(name = "discount_percent")
    private Integer discountPercent=0;

    @Column(name = "discount_price")
    private BigDecimal discountPrice= BigDecimal.valueOf(0);

    @Column(name = "number_using")
    private Integer numberUsing=1;

    @Column(name = "number_used")
    private Integer numberUsed=0;
}
