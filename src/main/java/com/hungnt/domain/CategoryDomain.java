package com.hungnt.domain;

import com.hungnt.config.Constants;
import com.hungnt.security.SecurityUtils;
import com.hungnt.web.rest.vm.category.CategoryRequestVM;
import java.io.Serializable;
import java.time.Instant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@Entity
@Table(name = "categories")
public class    CategoryDomain extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long id;

    @NotBlank
    @Column(name = "category_name")
    private String categoryName;

    @NotBlank
    @Column(name = "category_code")
    private String categoryCode;

    @Column(name = "description")
    private String description;

    @Column(name = "status")
    private String status;

    public CategoryDomain update(CategoryRequestVM categoryRequestVM){
        setCategoryCode(categoryRequestVM.getCategoryCode());
        setCategoryName(categoryRequestVM.getCategoryName());
        setDescription(categoryRequestVM.getDescription());
        setLastModifiedBy(SecurityUtils.getCurrentUserLogin().orElse(Constants.SYSTEM_ACCOUNT));
        setLastModifiedDate(Instant.now());
        return this;
    }
}
