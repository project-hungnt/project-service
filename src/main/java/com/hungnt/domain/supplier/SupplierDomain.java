package com.hungnt.domain.supplier;

import com.hungnt.config.Constants;
import com.hungnt.domain.BaseDomain;
import com.hungnt.security.SecurityUtils;
import com.hungnt.web.rest.vm.supplier.SupplierVM;
import java.io.Serializable;
import java.time.Instant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@Entity
@Table(name = "suppliers")
public class SupplierDomain extends BaseDomain implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    @Column(name = "code")
    @NotBlank
    @Size(max = 55)
    private String code;

    @Column(name = "name")
    @Size(max = 255)
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "contact_phone")
    @NotBlank
    @Size(max = 255)
    private String contactPhone;

    @Column(name = "contact_name")
    @NotBlank
    @Size(max = 255)
    private String contactName;

    @Column(name = "contact_email")
    @NotBlank
    @Size(max = 255)
    private String contactEmail;

    @Column(name = "city_code")
    @Size(max = 255)
    private String cityCode;

    @Column(name = "ward_code")
    @Size(max = 255)
    private String wardCode;

    @Column(name = "district_code")
    private String districtCode;

    @Column(name = "city_name")
    @Size(max = 255)
    private String cityName;

    @Column(name = "ward_name")
    @Size(max = 255)
    private String wardName;

    @Column(name = "district_name")
    @Size(max = 255)
    private String districtName;

    @Column(name = "address")
    @Size(max = 255)
    private String address;

    @Column(name = "bank_code")
    @NotBlank
    @Size(max = 255)
    private String bankCode;

    @Column(name = "bank_name")
    private String bankName;

    @Column(name = "account_number")
    @NotBlank
    @Size(max = 255)
    private String accountNumber;

    @Column(name = "account_name")
    @NotBlank
    @Size(max = 255)
    private String accountName;

    @Column(name = "bank_branch")
    @NotBlank
    @Size(max = 255)
    private String bankBranch;

    @Column(name = "status")
    private String status = Constants.STATUS_ACTIVE;

    public SupplierDomain update(SupplierVM supplierVM) {
        this.setAccountName(supplierVM.getAccountName());
        this.setAddress(supplierVM.getAddress());
        this.setCityCode(supplierVM.getCityCode());
        this.setAccountName(supplierVM.getAccountName());
        this.setBankBranch(supplierVM.getBankBranch());
        this.setBankCode(supplierVM.getBankCode());
        this.setDescription(supplierVM.getDescription());
        this.setCityName(supplierVM.getCityName());
        this.setContactEmail(supplierVM.getContactEmail());
        this.setContactName(supplierVM.getContactName());
        this.setContactPhone(supplierVM.getContactPhone());
        this.setName(supplierVM.getName());
        this.setDistrictCode(supplierVM.getDistrictCode());
        this.setDistrictName(supplierVM.getDistrictName());
        this.setWardCode(supplierVM.getWardCode());
        this.setWardName(supplierVM.getWardName());
        this.setLastModifiedBy(
            SecurityUtils.getCurrentUserLogin().orElse(Constants.SYSTEM_ACCOUNT));
        this.setLastModifiedDate(Instant.now());
        return this;
    }
}
