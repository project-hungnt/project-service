package com.hungnt.domain;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "wards")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Data
@RequiredArgsConstructor
public class WardDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long id;

    @NotBlank
    @Size(max = 255)
    @Column(name = "city_code", nullable = false)
    private String cityCode;

    @NotBlank
    @Size(max = 255)
    @Column(name = "district_code", nullable = false)
    private String districtCode;

    @NotBlank
    @Size(max = 255)
    @Column(name = "ward_name", nullable = false)
    private String wardName;

    @NotBlank
    @Size(max = 255)
    @Column(name = "ward_code", nullable = false)
    private String wardCode;
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof WardDomain)) {
            return false;
        }
        return id != null && id.equals(((WardDomain) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Ward{" +
            "id=" + id +
            ", cityCode='" + cityCode + '\'' +
            ", districtCode='" + districtCode + '\'' +
            ", wardName='" + wardName + '\'' +
            ", wardCode='" + wardCode + '\'' +
            '}';
    }
}
