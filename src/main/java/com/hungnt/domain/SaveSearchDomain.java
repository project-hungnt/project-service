package com.hungnt.domain;

import com.hungnt.config.Constants;
import com.hungnt.security.SecurityUtils;
import com.hungnt.web.rest.vm.saveSearch.SaveSearchVM;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "save_searches")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Data
@RequiredArgsConstructor
public class SaveSearchDomain extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long id;

    @NotBlank
    @Size(max = 50)
    @Column(name = "type", length = 50, nullable = false)
    private String type;

    @NotBlank
    @Size(max = 255)
    @Column(name = "name", nullable = false)
    private String name;

    @NotBlank
    @Size(max = 10000)
    @Column(name = "json_content", length = 10000, nullable = false)
    private String jsonContent;

    public SaveSearchDomain update(SaveSearchVM searchVM){
        setJsonContent(searchVM.getJsonContent());
        setType(searchVM.getType());
        setName(searchVM.getName());
        setLastModifiedDate(Instant.now());
        setLastModifiedBy(SecurityUtils.getCurrentUserLogin().orElse(Constants.SYSTEM_ACCOUNT));
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SaveSearchDomain)) {
            return false;
        }
        return id != null && id.equals(((SaveSearchDomain) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "SavedSearch{" +
            "id=" + id +
            ", type=" + type +
            ", name='" + name + '\'' +
            ", jsonFilter='" + jsonContent + '\'' +
            '}';
    }
}
