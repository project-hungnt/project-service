package com.hungnt.domain.qr;

import com.hungnt.config.Constants;
import com.hungnt.domain.BaseDomain;
import com.hungnt.security.SecurityUtils;
import com.hungnt.web.rest.vm.qr.BankVM;
import java.time.Instant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "banks")
public class BankDomain extends BaseDomain {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Column(name = "name")
    private String name;

    @NotBlank
    @Column(name = "code")
    private String code;

    @NotBlank
    @Column(name = "bin")
    private String bin;

    @Column(name ="is_transfer" )
    private int isTransfer;

    @NotBlank
    @Column(name = "short_name")
    private String shortName;

    @NotBlank
    @Column(name = "logo")
    private String logo;

    @Column(name = "support")
    private int support;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getBin() {
        return bin;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }

    public int getIsTransfer() {
        return isTransfer;
    }

    public void setIsTransfer(int isTransfer) {
        this.isTransfer = isTransfer;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public int getSupport() {
        return support;
    }

    public void setSupport(int support) {
        this.support = support;
    }

    public BankDomain update(BankVM bankVM){
        setBin(bankVM.getBin());
        setCode(bankVM.getCode());
        setIsTransfer(bankVM.getIsTransfer());
        setLogo(bankVM.getLogo());
        setName(bankVM.getName());
        setShortName(bankVM.getShortName());
        setSupport(bankVM.getSupport());
        setLastModifiedDate(Instant.now());
        setLastModifiedBy(SecurityUtils.getCurrentUserLogin().orElse(Constants.SYSTEM_ACCOUNT));
        return this;
    }
}
