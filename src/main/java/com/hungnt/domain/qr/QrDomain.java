package com.hungnt.domain.qr;

import com.hungnt.config.Constants;
import com.hungnt.domain.BaseDomain;
import com.hungnt.security.SecurityUtils;
import com.hungnt.web.rest.vm.qr.CreateQrVM;
import java.math.BigDecimal;
import java.time.Instant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Entity
@Table(name = "created_qr")
@Data @RequiredArgsConstructor
public class QrDomain extends BaseDomain {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Column(name = "bank_id")
    private String bankId;

    @NotBlank
    @Column(name = "account_number")
    private String accountNumber;

    @NotBlank
    @Column(name = "template")
    private String template;

    @Column(name = "amout")
    private BigDecimal amount;

    @Column(name = "description")
    private String description;

    @Column(name = "account_name")
    private String accountName;

    @Column(name = "link")
    private String link;

    @Column(name = "reference_id")
    private Long referenceId;

    @Override
    public String toString() {
        return "VietQrVM{" +
            "bankId='" + bankId + '\'' +
            ", accountNumber='" + accountNumber + '\'' +
            ", template='" + template + '\'' +
            ", amount=" + amount +
            ", description='" + description + '\'' +
            ", accountName='" + accountName + '\'' +
            '}';
    }
    public QrDomain update(CreateQrVM createQrVM){
        setAccountName(createQrVM.getAccountName());
        setBankId(createQrVM.getBankId());
        setDescription(createQrVM.getDescription());
        setAccountNumber(createQrVM.getAccountNumber());
        setTemplate(createQrVM.getTemplate());
        setAmount(createQrVM.getAmount());
        setReferenceId(createQrVM.getReferenceId());
        setLastModifiedDate(Instant.now());
        setLastModifiedBy(SecurityUtils.getCurrentUserLogin().orElse(Constants.SYSTEM_ACCOUNT));
        return this;
    }
}
