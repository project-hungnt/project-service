package com.hungnt.domain;

import com.hungnt.config.Constants;
import com.hungnt.security.SecurityUtils;
import com.hungnt.utils.TextUtils;
import com.hungnt.web.rest.vm.category.CategoryRequestVM;
import com.hungnt.web.rest.vm.location.LocationRequestVM;
import java.io.Serializable;
import java.time.Instant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import liquibase.pro.packaged.lo;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;

@Data
@Entity
@Table(name = "locations")
@RequiredArgsConstructor
public class LocationDomain extends BaseDomain implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "city_code")
    private String cityCode;

    @Column(name = "ward_code")
    private String wardCode;

    @Column(name = "district_code")
    private String districtCode;

    @Column(name = "city_name")
    private String cityName;

    @Column(name = "ward_name")
    private String wardName;

    @Column(name = "district_name")
    private String districtName;

    @Column(name = "address")
    private String address;


    @Column(name = "status")
    private String status = Constants.STATUS_ACTIVE;

    public void setCityCode(String cityCode) {
        this.cityCode = TextUtils.generatePropertyCode(cityCode);
    }

    public void setWardCode(String wardCode) {
        this.wardCode = TextUtils.generatePropertyCode(wardCode);
    }

    public void setDistrictCode(String districtCode) {
        this.districtCode = TextUtils.generatePropertyCode(districtCode);
    }

    public LocationDomain update(LocationRequestVM locationRequestVM){
        setAddress(locationRequestVM.getAddress());
        setName(locationRequestVM.getName());
        setStatus(StringUtils.isNotBlank(locationRequestVM.getStatus())?locationRequestVM.getStatus():this.status);
        setCityCode(locationRequestVM.getCityName());
        setDistrictCode(locationRequestVM.getDistrictName());
        setWardCode(locationRequestVM.getWardName());
        setCityName(locationRequestVM.getCityName());
        setDistrictName(locationRequestVM.getDistrictName());
        setWardName(locationRequestVM.getWardName());
        setLastModifiedBy(SecurityUtils.getCurrentUserLogin().orElse(Constants.SYSTEM_ACCOUNT));
        setLastModifiedDate(Instant.now());
        return this;
    }
    public LocationDomain crate(LocationDomain locationDomain){
        setCityCode(locationDomain.getCityName());
        setDistrictCode(locationDomain.getDistrictName());
        setWardCode(locationDomain.getWardName());
        return this;
    }
}
