package com.hungnt.service.common;

import com.hungnt.config.Constants;
import com.hungnt.domain.FileUpload;
import com.hungnt.domain.product.ProductDetailDomain;
import com.hungnt.domain.product.ProductDomain;
import com.hungnt.repository.FileUploadRepository;
import com.hungnt.repository.product.ProductDetailRepository;
import com.hungnt.repository.product.ProductRepository;
import com.hungnt.web.rest.errors.FormValidateException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class FileUpLoadService {

    private final FileUploadRepository uploadRepository;
    private final ProductRepository productRepository;
    private final ProductDetailRepository productDetailRepository;

    public FileUpLoadService(FileUploadRepository uploadRepository,
        ProductRepository productRepository,
        ProductDetailRepository productDetailRepository) {
        this.uploadRepository = uploadRepository;
        this.productRepository = productRepository;
        this.productDetailRepository = productDetailRepository;
    }

    public List<FileUpload> getAllFileByProvider(String provider, Long referentId) {
        List<FileUpload> fileUploads;
        switch (provider) {
            case Constants.PRODUCT_IMAGE:
                fileUploads = getAllFileProduct(referentId);
                break;
            default:
                fileUploads = uploadRepository.findAllByProviderAndReferenceId(provider.toUpperCase(), referentId);
        }
        return fileUploads
            .stream()
            .filter(Objects::nonNull)
            .filter(x -> x.isDeleted() == false)
            .collect(Collectors.toList());
    }

    public List<FileUpload> getAllFileProduct(Long referentId) {
        List<FileUpload> fileUploads;
        Optional<ProductDomain> productDomain = productRepository.findById(referentId);
        if (productDomain.isEmpty()){
            throw new FormValidateException("","Không tìm thấy sản phẩm");
        }
        fileUploads = uploadRepository.findAllByProviderAndReferenceId(Constants.PRODUCT_IMAGE.toUpperCase(), productDomain.get().getId());
        List<ProductDetailDomain>productDetailDomains = productDetailRepository.findAllByProductId(productDomain.get().getId());
        if(!productDetailDomains.isEmpty()){
            productDetailDomains.forEach(item->{
                List<FileUpload> fileProductDetail = uploadRepository.findAllByProviderAndReferenceId(Constants.PRODUCT_DETAIL_IMAGE.toUpperCase(),item.getId());
                if(!fileProductDetail.isEmpty()){
                    fileUploads.addAll(fileProductDetail);
                }
            });
        }
        return fileUploads;
    }
}
