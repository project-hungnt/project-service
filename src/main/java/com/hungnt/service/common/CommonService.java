package com.hungnt.service.common;

import io.github.jhipster.service.filter.BigDecimalFilter;
import io.github.jhipster.service.filter.InstantFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import java.math.BigDecimal;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.time.Instant;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;

public class CommonService {
    public static String builderQuery(String query){
        StringBuilder stringBuilder = new StringBuilder();
        if (StringUtils.isNotBlank(query)) {
            CharacterIterator it = new StringCharacterIterator(query);
            for (char ch = it.first(); ch != CharacterIterator.DONE; ch = it.next()) {
                switch (ch) {
                    case '%':
                        stringBuilder.append("\\");
                        stringBuilder.append("%");
                        break;
                    case '_':
                        stringBuilder.append("\\");
                        stringBuilder.append("_");
                        break;
                    default:
                        stringBuilder.append(ch);
                        break;
                }
            }
        }
        return stringBuilder.toString();
    }
    public static Optional<InstantFilter> buildDateMinMaxFilter(Instant minDate, Instant maxDate){
        if (minDate == null && maxDate == null) {
            return Optional.empty();
        }
        InstantFilter instantFilter = new InstantFilter();
        if (minDate != null) {
            instantFilter.setGreaterThanOrEqual(minDate);
        }
        if (maxDate != null) {
            instantFilter.setLessThanOrEqual(maxDate);
        }
        return Optional.of(instantFilter);
    }

    public static Optional<BigDecimalFilter> buildBigDecimalMinMax(BigDecimal min, BigDecimal max) {
        if (min != null || max != null) {
            BigDecimalFilter bigDecimalFilter = new BigDecimalFilter();
            if (min != null) {
                bigDecimalFilter.setGreaterThanOrEqual(min);
            }
            if (max != null) {
                bigDecimalFilter.setLessThanOrEqual(max);
            }
            return Optional.of(bigDecimalFilter);
        } else {
            return Optional.empty();
        }
    }
    public static Optional<IntegerFilter> buildIntegerMinMax(Integer min, Integer max) {
        if (min != null || max != null) {
            IntegerFilter bigDecimalFilter = new IntegerFilter();
            if (min != null) {
                bigDecimalFilter.setGreaterThanOrEqual(min);
            }
            if (max != null) {
                bigDecimalFilter.setLessThanOrEqual(max);
            }
            return Optional.of(bigDecimalFilter);
        } else {
            return Optional.empty();
        }
    }
}
