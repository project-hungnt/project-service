package com.hungnt.service.supplier;

import com.hungnt.domain.customer.CustomerDomain;
import com.hungnt.domain.supplier.SupplierDomain;
import com.hungnt.domain.supplier.SupplierDomain_;
import com.hungnt.repository.SupplierRepository;
import com.hungnt.service.common.CommonService;
import com.hungnt.service.dto.filter.SupplierCriteria;
import com.hungnt.utils.TextUtils;
import com.hungnt.web.rest.errors.FormValidateException;
import com.hungnt.web.rest.vm.filter.CustomerFilter;
import com.hungnt.web.rest.vm.filter.SupplierFilter;
import io.github.jhipster.service.QueryService;
import io.github.jhipster.service.filter.StringFilter;
import java.util.Optional;
import java.util.function.Function;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class SupplierService extends QueryService<SupplierDomain>{
    private final SupplierRepository supplierRepository;

    public SupplierService(SupplierRepository supplierRepository) {
        this.supplierRepository = supplierRepository;
    }

    public SupplierDomain save(SupplierDomain supplierDomain){
        String code = TextUtils.getCode("SUP",supplierRepository.count());
        supplierDomain.setCode(code);
        return supplierRepository.save(supplierDomain);
    }
    @Transactional(readOnly = true)
    public SupplierDomain getById(Long id){
        Optional<SupplierDomain> optionalSupplierDomain = supplierRepository.findById(id);
        if(optionalSupplierDomain.isEmpty()){
            throw new FormValidateException("error","Không tìm thấy nhà cũng cấp");

        }
        return optionalSupplierDomain.get();
    }
    @Transactional(readOnly = true)
    public SupplierDomain getByCode(String code){
        Optional<SupplierDomain> optionalSupplierDomain = supplierRepository.findByCode(code);
        if(optionalSupplierDomain.isEmpty()){
            throw new FormValidateException("error","Không tìm thấy nhà cũng cấp");

        }
        return optionalSupplierDomain.get();
    }
    @Transactional(readOnly = true)
    public Page<SupplierDomain> findByCriteria(SupplierFilter filter, Pageable page) {
        final Specification<SupplierDomain> specification = createSpecification(buildCriteria(filter));
        return supplierRepository.findAll(specification, page);
    }
    public SupplierCriteria buildCriteria(SupplierFilter filter) {
        SupplierCriteria criteria = new SupplierCriteria();

        if (CollectionUtils.isNotEmpty(filter.getStatuses())) {
            StringFilter statusFilter = new StringFilter();
            statusFilter.setIn(filter.getStatuses());
            criteria.setStatuses(statusFilter);
        }
        if (CollectionUtils.isNotEmpty(filter.getCities())) {
            StringFilter stringFilter = new StringFilter();
            stringFilter.setIn(filter.getCities());
            criteria.setCities(stringFilter);
        }
        criteria.setQuery(CommonService.builderQuery(filter.getQuery()));
        CommonService.buildDateMinMaxFilter(filter.getCreatedDateMin(), filter.getCreatedDateMax())
            .ifPresent(criteria::setCreatedDate);
        CommonService
            .buildDateMinMaxFilter(filter.getLastModifiedDateMin(), filter.getLastModifiedDateMax())
            .ifPresent(criteria::setLastModifiedDate);
        return criteria;
    }

    protected Specification<SupplierDomain> createSpecification(SupplierCriteria criteria) {
        Specification<SupplierDomain> specification = Specification.where(null);
        if (criteria != null) {

            if (criteria.getStatuses() != null) {
                assert specification != null;
                specification = specification.and(
                    buildSpecification(criteria.getStatuses(), SupplierDomain_.status));
            }
            if (criteria.getCities() != null) {
                assert specification != null;
                specification = specification.and(
                    buildSpecification(criteria.getCities(), SupplierDomain_.cityCode));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(),
                    SupplierDomain_.createdDate));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(
                    buildRangeSpecification(criteria.getLastModifiedDate(),
                        SupplierDomain_.lastModifiedDate));
            }
            if (criteria.getQuery() != null) {
                specification = specification.and(createQuerySpecification(criteria.getQuery()));
            }
        }
        return specification;
    }

    private Specification<SupplierDomain> createQuerySpecification(String query) {
        if (StringUtils.isBlank(query)) {
            return null;
        }
        query = query.trim();
        return Specification
            .where(likeSpecification(root -> root.get(SupplierDomain_.name), query))
            .or(likeSpecification(root -> root.get(SupplierDomain_.code), query));
    }

    protected Specification<SupplierDomain> likeSpecification(
        Function<Root<SupplierDomain>, Expression<String>> rootExpressionFunction,
        final String value) {
        return (root, query, builder) -> builder
            .like(rootExpressionFunction.apply(root), "%" + value + "%");
    }
}
