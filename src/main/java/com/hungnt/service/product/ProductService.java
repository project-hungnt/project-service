package com.hungnt.service.product;

import com.hungnt.config.Constants;
import com.hungnt.domain.FileUpload;
import com.hungnt.domain.product.ProductDetailDomain;
import com.hungnt.domain.product.ProductDetailDomain_;
import com.hungnt.domain.product.ProductDomain;
import com.hungnt.domain.product.ProductDomain_;
import com.hungnt.firebase.FirebaseInitializeApp;
import com.hungnt.repository.CategoryRepository;
import com.hungnt.repository.FileUploadRepository;
import com.hungnt.repository.LocationRepository;
import com.hungnt.repository.product.ProductRepository;
import com.hungnt.service.common.CommonService;
import com.hungnt.service.dto.filter.product.ProductCriteria;
import com.hungnt.utils.TextUtils;
import com.hungnt.web.rest.errors.FormValidateException;
import com.hungnt.web.rest.vm.fileUpload.FileRequest;
import com.hungnt.web.rest.vm.fileUpload.FileRequestWrap;
import com.hungnt.web.rest.vm.filter.ProductFilter;
import com.hungnt.web.rest.vm.product.ProductDetailUpdateRequest;
import com.hungnt.web.rest.vm.product.ProductRequestDetailVM;
import com.hungnt.web.rest.vm.product.ProductRequestVM;
import com.hungnt.web.rest.vm.product.ProductUpdateRequest;
import com.hungnt.web.rest.vm.product.UpdateStatusVM;
import io.github.jhipster.service.QueryService;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;
import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MapperFacade;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
@Slf4j
public class ProductService extends QueryService<ProductDomain> {

    private final ProductRepository productRepository;
    private final FileUploadRepository uploadRepository;
    private final ProductDetailService productDetailService;
    private final FirebaseInitializeApp firebaseInitializeApp;
    private final CategoryRepository categoryRepository;
    private final LocationRepository locationRepository;

    public ProductService(ProductRepository productRepository,
        FileUploadRepository uploadRepository,
        ProductDetailService productDetailService,
        FirebaseInitializeApp firebaseInitializeApp,
        CategoryRepository categoryRepository,
        LocationRepository locationRepository) {
        this.productRepository = productRepository;
        this.uploadRepository = uploadRepository;
        this.productDetailService = productDetailService;
        this.firebaseInitializeApp = firebaseInitializeApp;
        this.categoryRepository = categoryRepository;
        this.locationRepository = locationRepository;
    }
    @Transactional(readOnly = true)
    public Page<ProductDomain> findByCriteria(ProductFilter filter, Pageable page) {
        log.info("find by filter : {}, page: {}", filter, page);
        final Specification<ProductDomain> specification = createSpecification(
            buildCriteria(filter));
        return productRepository.findAll(specification, page);
    }
    public ProductCriteria buildCriteria(ProductFilter filter) {
        ProductCriteria criteria = new ProductCriteria();
        if (CollectionUtils.isNotEmpty(filter.getCategories())) {
            LongFilter stringFilter = new LongFilter();
            stringFilter.setIn(filter.getCategories());
            criteria.setCategoryIds(stringFilter);
        }
        StringFilter statusFilter = new StringFilter();
        if (CollectionUtils.isNotEmpty(filter.getStatus())) {
            statusFilter.setIn(filter.getStatus());
        }
        statusFilter.setNotIn(Collections.singletonList(Constants.STATUS_DELETED));
        criteria.setStatus(statusFilter);
        if (CollectionUtils.isNotEmpty(filter.getLocations())) {
            LongFilter stringFilter = new LongFilter();
            stringFilter.setIn(filter.getLocations());
            criteria.setLocationIds(stringFilter);
        }
        criteria.setQuery(CommonService.builderQuery(filter.getQuery()));
        CommonService.buildDateMinMaxFilter(filter.getCreatedDateMin(), filter.getCreatedDateMax())
            .ifPresent(criteria::setCreatedDate);
        CommonService
            .buildDateMinMaxFilter(filter.getLastModifiedDateMin(), filter.getLastModifiedDateMax())
            .ifPresent(criteria::setLastModifiedDate);
        return criteria;
    }

    protected Specification<ProductDomain> createSpecification(ProductCriteria criteria) {
        Specification<ProductDomain> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getCategoryIds() != null) {
                assert specification != null;
                specification = specification.and(
                    buildSpecification(criteria.getCategoryIds(), ProductDomain_.categoryId));
            }
            if (criteria.getLocationIds() != null) {
                assert specification != null;
                specification = specification.and(
                    buildSpecification(criteria.getLocationIds(), ProductDomain_.locationId));
            }
            if (criteria.getStatus() != null) {
                assert specification != null;
                specification = specification.and(
                    buildSpecification(criteria.getStatus(), ProductDomain_.status));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(),
                    ProductDetailDomain_.createdDate));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(
                    buildRangeSpecification(criteria.getLastModifiedDate(),
                        ProductDetailDomain_.lastModifiedDate));
            }
            if (criteria.getQuery() != null) {
                specification = specification.and(createQuerySpecification(criteria.getQuery()));
            }
        }
        return specification;
    }

    private Specification<ProductDomain> createQuerySpecification(String query) {
        if (StringUtils.isBlank(query)) {
            return null;
        }
        query = query.trim();
        return Specification
            .where(likeSpecification(root -> root.get(ProductDomain_.productName), query))
            .or(likeSpecification(root -> root.get(ProductDomain_.productCode), query));
    }

    protected Specification<ProductDomain> likeSpecification(
        Function<Root<ProductDomain>, Expression<String>> rootExpressionFunction,
        final String value) {
        return (root, query, builder) -> builder
            .like(rootExpressionFunction.apply(root), "%" + value + "%");
    }
    @Transactional
    public ProductDomain create (ProductRequestVM productRequestVM)
        throws IOException, NotFoundException {
        ProductDomain productDomain = productRepository.save(buildProduct(productRequestVM));
        uploadFileProduct(productDomain, productRequestVM.getFiles());
        if(productRequestVM.getProductDetails() !=null && productRequestVM.getProductDetails().size()>0){
            productRequestVM.getProductDetails().forEach(item->{
                try {
                    productDetailService.createProductDetail(productDomain,item);
                } catch (IOException e) {
                    log.info("co loi xay ra khi them productdetail {}",e.getMessage());
                    e.printStackTrace();
                }
            });
        }else{
            try {
                ProductRequestDetailVM requestDetailVM = new ProductRequestDetailVM();
                requestDetailVM.setPriceSell(productRequestVM.getPriceSell());
                requestDetailVM.setQuantity(productRequestVM.getQuantity());
                requestDetailVM.setPrice(productRequestVM.getPrice());
                requestDetailVM.setProductDetailName(productDomain.getProductName());
                productDetailService.createProductDetail(productDomain,requestDetailVM);
            } catch (IOException e) {
                log.info("co loi xay ra khi them productdetail {}",e.getMessage());
                e.printStackTrace();
            }
        }
        return productDomain;
    }
    public ProductDomain buildProduct (ProductRequestVM productRequestVM) throws NotFoundException {
        String productCode = TextUtils.getCode("P",productRepository.count());
        if(productRequestVM.getCategoryId()!=null && productRequestVM.getCategoryId()>0){
            categoryRepository.findById(productRequestVM.getCategoryId()).orElseThrow(
                NotFoundException::new);
        }
        if(productRequestVM.getLocationId()!=null && productRequestVM.getLocationId()>0){
            locationRepository.findById(productRequestVM.getLocationId()).orElseThrow(
                NotFoundException::new);
        }
        ProductDomain productDomain =  new ProductDomain();
        productDomain.setLocationId(productRequestVM.getLocationId());
        productDomain.setCategoryId(productRequestVM.getCategoryId());
        productDomain.setDescription(productRequestVM.getDescription());
        productDomain.setProductName(productRequestVM.getProductName());
        productDomain.setStatus(productRequestVM.getStatus());
        productDomain.setProductCode(productCode);
        return productDomain;
    }
    @Transactional
    private List<FileUpload> uploadFileProduct (Long id,String provider,List<FileRequest> files)
        throws IOException {
        FileRequestWrap fileRequestWrap = new FileRequestWrap();
        fileRequestWrap.setFileRequests(files);
        fileRequestWrap.setReferenceId(id);
        fileRequestWrap.setProvider(provider);
        List<FileUpload> fileUploads = firebaseInitializeApp.uploadFile(fileRequestWrap);
        return  uploadRepository.saveAll(fileUploads);
    }
    public ProductDomain findByCode(String code){
        Optional<ProductDomain>optionalProductDomain = productRepository.findByProductCode(code);
        if(optionalProductDomain.isEmpty() || optionalProductDomain.get().getStatus().equals(Constants.STATUS_DELETED)){
            throw new FormValidateException("","Không tìm thấy sản phẩm với mã là "+code);
        }
        return optionalProductDomain.get();
    }
    public ProductDomain findById(Long id){
        Optional<ProductDomain>optionalProductDomain = productRepository.findById(id);
        if(optionalProductDomain.isEmpty() || optionalProductDomain.get().getStatus().equals(Constants.STATUS_DELETED)){
            throw new FormValidateException("","Không tìm thấy sản phẩm với mã là "+id);
        }
        return optionalProductDomain.get();
    }
    @Transactional
    public String updateStatus(UpdateStatusVM updateStatusVM){
        if(updateStatusVM.getIds()!=null && updateStatusVM.getIds().length>0){
            for (Long id : updateStatusVM.getIds()) {
                Optional<ProductDomain>optionalProductDomain = productRepository.findById(id);
                if(optionalProductDomain.isEmpty()){
                    throw new FormValidateException("error","Không tìm thấy sản phẩm với mã là "+id);
                }
                ProductDomain productDomain = optionalProductDomain.get();
                updateStatusProduct(productDomain,updateStatusVM.getStatus());
            }
        }
        return updateStatusVM.getStatus();
    }
    @Transactional
    public void updateStatusProduct ( ProductDomain productDomain , String status){
        productDomain.setStatus(status);
        productRepository.save(productDomain);
        if(!status.equals(Constants.STATUS_ACTIVE)){
            List<ProductDetailDomain> productDetailDomains = productDetailService.finAllByProductId(productDomain.getId());
            if(!productDetailDomains.isEmpty()){
                productDetailDomains.forEach(productDetailDomain -> {
                    productDetailDomain.setStatus(productDomain.getStatus());
                    productDetailService.save(productDetailDomain);
                });
            }
        }
    }
    @Transactional
    public ProductDomain update (Long id,ProductUpdateRequest productUpdateRequest)
        throws IOException {
        ProductDomain productDomain =findById(id).update(productUpdateRequest);
        productDomain=productRepository.save(productDomain);
        uploadFileProduct(productDomain, productUpdateRequest.getFiles());
        if(productUpdateRequest.getProductDetails().size()>0){
            productUpdateRequest.getProductDetails().forEach(productDetailUpdateRequest -> {
                productDetailService.update(productDetailUpdateRequest);
            });
        }
        return productDomain;
    }

    private void uploadFileProduct(ProductDomain productDomain, List<FileRequest> files) throws IOException {
        if(files !=null){
            List<FileUpload> fileUploads = uploadFileProduct(productDomain.getId(), Constants.PRODUCT_IMAGE,
                files);
            if(!fileUploads.isEmpty()){
                productDomain.setDefaultImage(fileUploads.get(0).getFileUrl());
                productRepository.save(productDomain);
            }
        }
    }
    public List<ProductDomain> findAllByCategoryId(Long id){
        return productRepository.findAllByCategoryId(id).stream().filter(Objects::nonNull).filter(productDomain -> !productDomain.getStatus().equals(Constants.STATUS_DELETED)).collect(
            Collectors.toList());
    }
}
