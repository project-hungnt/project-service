package com.hungnt.service.product;

import com.hungnt.config.Constants;
import com.hungnt.domain.FileUpload;
import com.hungnt.domain.product.ProductDetailDomain;
import com.hungnt.domain.product.ProductDetailDomain_;
import com.hungnt.domain.product.ProductDomain;
import com.hungnt.firebase.FirebaseInitializeApp;
import com.hungnt.repository.FileUploadRepository;
import com.hungnt.repository.product.ProductDetailRepository;
import com.hungnt.service.common.CommonService;
import com.hungnt.service.dto.filter.product.ProductDetailCriteria;
import com.hungnt.utils.TextUtils;
import com.hungnt.web.rest.errors.FormValidateException;
import com.hungnt.web.rest.vm.fileUpload.FileCreateVM;
import com.hungnt.web.rest.vm.filter.ProductDetailFilter;
import com.hungnt.web.rest.vm.product.ProductDetailUpdateRequest;
import com.hungnt.web.rest.vm.product.UpdateStatusVM;
import com.hungnt.web.rest.vm.product.ProductRequestDetailVM;
import io.github.jhipster.service.QueryService;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
public class ProductDetailService extends QueryService<ProductDetailDomain> {

    private final ProductDetailRepository productDetailRepository;
    private final FirebaseInitializeApp firebaseInitializeApp;
    private final FileUploadRepository uploadRepository;
    public ProductDetailService(
        ProductDetailRepository productDetailRepository,
        FirebaseInitializeApp firebaseInitializeApp,
        FileUploadRepository uploadRepository) {
        this.productDetailRepository = productDetailRepository;
        this.firebaseInitializeApp = firebaseInitializeApp;
        this.uploadRepository = uploadRepository;
    }

    @Transactional(readOnly = true)
    public Page<ProductDetailDomain> findByCriteria(ProductDetailFilter filter, Pageable page) {
        log.info("find by filter : {}, page: {}", filter, page);
        final Specification<ProductDetailDomain> specification = createSpecification(
            buildCriteria(filter));
        return productDetailRepository.findAll(specification, page);
    }

    @Transactional
    public ProductDetailDomain save(ProductDetailDomain productDetailDomain) {
        return productDetailRepository.save(productDetailDomain);
    }

    public ProductDetailCriteria buildCriteria(ProductDetailFilter filter) {
        ProductDetailCriteria criteria = new ProductDetailCriteria();
        if (CollectionUtils.isNotEmpty(filter.getCategories())) {
            LongFilter stringFilter = new LongFilter();
            stringFilter.setIn(filter.getCategories());
            criteria.setCategoryIds(stringFilter);
        }
        if ((CollectionUtils.isNotEmpty(filter.getLocationIds()))) {
            LongFilter stringFilter = new LongFilter();
            stringFilter.setIn(filter.getLocationIds());
            criteria.setLocationIds(stringFilter);
        }
        StringFilter statusFilter = new StringFilter();
        if (CollectionUtils.isNotEmpty(filter.getStatus())) {
            statusFilter.setIn(filter.getStatus());
        }
        statusFilter.setNotIn(Collections.singletonList(Constants.STATUS_DELETED));
        criteria.setStatus(statusFilter);
        criteria.setQuery(CommonService.builderQuery(filter.getQuery()));
        CommonService.buildDateMinMaxFilter(filter.getCreatedDateMin(), filter.getCreatedDateMax())
            .ifPresent(criteria::setCreatedDate);
        CommonService
            .buildDateMinMaxFilter(filter.getLastModifiedDateMin(), filter.getLastModifiedDateMax())
            .ifPresent(criteria::setLastModifiedDate);
        CommonService.buildBigDecimalMinMax(filter.getPriceMin(), filter.getPriceMax())
            .ifPresent(criteria::setPrices);
        CommonService.buildBigDecimalMinMax(filter.getPriceSellMin(), filter.getPriceSellMax())
            .ifPresent(criteria::setPrices);
        CommonService.buildIntegerMinMax(filter.getQuantityMin(), filter.getQuantityMax())
            .ifPresent(criteria::setQuantities);
        CommonService.buildIntegerMinMax(filter.getQuantitySellMin(), filter.getQuantitySellMax())
            .ifPresent(criteria::setQuantitySells);
        return criteria;
    }

    protected Specification<ProductDetailDomain> createSpecification(
        ProductDetailCriteria criteria) {
        Specification<ProductDetailDomain> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getCategoryIds() != null) {
                assert getSpecification(specification) != null;
                specification = getSpecification(specification).and(
                    buildSpecification(criteria.getCategoryIds(), ProductDetailDomain_.categoryId));
            }
            if (criteria.getLocationIds() != null) {
                assert getSpecification(specification) != null;
                specification = getSpecification(specification).and(
                    buildSpecification(criteria.getLocationIds(), ProductDetailDomain_.locationId));
            }
            if (criteria.getStatus() != null) {
                assert getSpecification(specification) != null;
                specification = getSpecification(specification).and(
                    buildSpecification(criteria.getStatus(), ProductDetailDomain_.status));
            }
            if (criteria.getPrices() != null) {
                specification = getSpecification(specification)
                    .and(buildRangeSpecification(criteria.getPrices(), ProductDetailDomain_.price));
            }
            if (criteria.getPriceSells() != null) {
                specification = getSpecification(specification)
                    .and(buildRangeSpecification(criteria.getPriceSells(),
                        ProductDetailDomain_.priceSell));
            }
            if (criteria.getQuantities() != null) {
                specification = getSpecification(specification)
                    .and(buildRangeSpecification(criteria.getQuantities(),
                        ProductDetailDomain_.quantity));
            }
            if (criteria.getQuantitySells() != null) {
                specification = getSpecification(specification).and(
                    buildRangeSpecification(criteria.getQuantitySells(),
                        ProductDetailDomain_.quantitySell));
            }
            if (criteria.getCreatedDate() != null) {
                specification = getSpecification(specification)
                    .and(buildRangeSpecification(criteria.getCreatedDate(),
                        ProductDetailDomain_.createdDate));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = getSpecification(specification).and(
                    buildRangeSpecification(criteria.getLastModifiedDate(),
                        ProductDetailDomain_.lastModifiedDate));
            }
            if (criteria.getQuery() != null) {
                specification = getSpecification(specification)
                    .and(createQuerySpecification(criteria.getQuery()));
            }
        }
        return getSpecification(specification);
    }

    private Specification<ProductDetailDomain> getSpecification(
        Specification<ProductDetailDomain> specification) {
        return specification;
    }

    private Specification<ProductDetailDomain> createQuerySpecification(String query) {
        if (StringUtils.isBlank(query)) {
            return null;
        }
        query = query.trim();
        return Specification
            .where(likeSpecification(root -> root.get(ProductDetailDomain_.barCode), query))
            .or(likeSpecification(root -> root.get(ProductDetailDomain_.size), query))
            .or(likeSpecification(root -> root.get(ProductDetailDomain_.color), query))
            .or(likeSpecification(root -> root.get(ProductDetailDomain_.productName), query))
            .or(likeSpecification(root -> root.get(ProductDetailDomain_.code), query))
            .or(likeSpecification(root -> root.get(ProductDetailDomain_.material), query));
    }

    protected Specification<ProductDetailDomain> likeSpecification(
        Function<Root<ProductDetailDomain>, Expression<String>> rootExpressionFunction,
        final String value) {
        return (root, query, builder) -> builder
            .like(rootExpressionFunction.apply(root), "%" + value + "%");
    }

    public ProductDetailDomain createProductDetail(ProductDomain productDomain,
        ProductRequestDetailVM item)
        throws IOException {
        String productCode = TextUtils.getCode("PD",productDetailRepository.count());
        ProductDetailDomain productDetailDomain = new ProductDetailDomain();
        productDetailDomain.setCode(productCode);
        productDetailDomain.setBarCode(item.getBarCode());
        productDetailDomain.setCategoryId(productDomain.getCategoryId());
        productDetailDomain.setColor(item.getColor());
        productDetailDomain.setSize(item.getSize());
        productDetailDomain.setDiscount(item.getDiscount());
        productDetailDomain.setQuantity(item.getQuantity());
        productDetailDomain.setQuantitySell(item.getQuantitySell());
        productDetailDomain.setLocationId(productDomain.getLocationId());
        productDetailDomain.setMaterial(item.getMaterial());
        productDetailDomain.setPrice(item.getPrice());
        productDetailDomain.setPriceSell(item.getPriceSell());
        productDetailDomain.setProductId(productDomain.getId());
        productDetailDomain.setProductCode(productDomain.getProductCode());
        productDetailDomain.setProductName(item.getProductDetailName());
        productDetailDomain.setStatus(productDomain.getStatus());
        productDetailDomain = productDetailRepository.save(productDetailDomain);
        if (item.getFileCreateVM() != null) {
            FileCreateVM fileCreateVM = item.getFileCreateVM();
            fileCreateVM.setProvider(Constants.PRODUCT_DETAIL_IMAGE);
            fileCreateVM.setReferentId(productDetailDomain.getId());
            FileUpload fileUpload = firebaseInitializeApp.uploadOnFile(fileCreateVM);
            fileUpload = uploadRepository.save(fileUpload);
            if (fileUpload != null) {
                productDetailDomain.setDefaultImage(fileUpload.getFileUrl());
                productDetailRepository.save(productDetailDomain);
            }
        }
        return productDetailDomain;
    }

    public List<ProductDetailDomain> finAllByProductId(Long productId) {
        return productDetailRepository.findAllByProductId(productId).stream()
            .filter(Objects::nonNull)
            .filter(x->!x.getStatus().equals(Constants.STATUS_DELETED))
            .collect(Collectors.toList());
    }

    @Transactional
    public String updateStatusProductDetail (UpdateStatusVM updateStatusVM){
        String status = updateStatusVM.getStatus()!=null? updateStatusVM.getStatus():Constants.STATUS_ACTIVE;
        if(updateStatusVM.getIds()!=null && updateStatusVM.getIds().length>0){
            for (Long id : updateStatusVM.getIds()) {
                Optional<ProductDetailDomain> productDetailDomain = productDetailRepository.findById(id);
                if(productDetailDomain.isPresent()){
                    ProductDetailDomain productDomain = productDetailDomain.get();
                    productDomain.setStatus(status);
                    productDetailRepository.save(productDomain);
                }
            }
        }
        return status;
    }

    public void updateQuantityProductDetail (Long id,int quantity){
        Optional<ProductDetailDomain> productDetailDomainOptional = productDetailRepository.findById(id);
        if (productDetailDomainOptional.isEmpty()){
            throw new FormValidateException("error","Không tìm thấy phiên bản sản phẩm");
        }
        ProductDetailDomain productDetailDomain = productDetailDomainOptional.get();
        productDetailDomain.setQuantity(productDetailDomain.getQuantity()-quantity);
        productDetailRepository.save(productDetailDomain);
    }
    public ProductDetailDomain finById(Long id){
        Optional<ProductDetailDomain> optionalProductDetailDomain = productDetailRepository.findById(id);
        if(optionalProductDetailDomain.isEmpty()){
            throw new FormValidateException("","Không tìm thấy phiên bản sản phẩm");
        }
        return optionalProductDetailDomain.get();
    }
    public ProductDetailDomain update(ProductDetailUpdateRequest productDetailUpdateRequest){
        ProductDetailDomain productDetailDomain = finById(productDetailUpdateRequest.getId()).update(productDetailUpdateRequest);
        return productDetailRepository.save(productDetailDomain);
    }

}
