package com.hungnt.service;


import com.hungnt.domain.User;
import com.hungnt.domain.User_;
import com.hungnt.repository.UserRepository;
import com.hungnt.service.common.CommonService;
import com.hungnt.service.dto.UserDTO;
import com.hungnt.service.dto.filter.UserCriteria;
import com.hungnt.web.rest.vm.filter.UserFilter;
import io.github.jhipster.service.QueryService;
import io.github.jhipster.service.filter.StringFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Function;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;
import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MapperFacade;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
@Slf4j
public class UserQueryService extends QueryService<User> {

    private final UserRepository userRepository;
    private final MapperFacade mapper;

    public UserQueryService(UserRepository userRepository,
                            MapperFacade mapper) {
        this.userRepository = userRepository;
        this.mapper = mapper;
    }

    @Transactional(readOnly = true)
    public long countByCriteria(UserCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<User> specification = createSpecification(criteria);
        return userRepository.count(specification);
    }

    @Transactional(readOnly = true)
    public Page<UserDTO> findByCriteria(UserFilter filter, Pageable page) {
        log.debug("find by criteria : {}, page: {}", filter, page);
        UserCriteria criteria = buildCriteria(filter);
        final Specification<User> specification = createSpecification(criteria);
        Page<User> userPage = userRepository.findAll(specification, page);
        return userPage.map(UserDTO::reconstruct);
    }

    public UserCriteria buildCriteria(UserFilter filter) {
        UserCriteria criteria = new UserCriteria();

        if (CollectionUtils.isNotEmpty(filter.getStatuses())) {
            StringFilter statusFilter = new StringFilter();
            statusFilter.setIn(filter.getStatuses());
            criteria.setStatuses(statusFilter);
        }
        if (CollectionUtils.isNotEmpty(filter.getRoleTypes())) {
            StringFilter statusFilter = new StringFilter();
            statusFilter.setIn(filter.getRoleTypes());
            criteria.setRoleTypes(statusFilter);
        }
        StringFilter statusFilter = new StringFilter();
        statusFilter.setNotIn(Arrays.asList(new String[]{"System"}));
        criteria.setPhoneNumber(statusFilter);
        criteria.setQuery(CommonService.builderQuery(filter.getQuery()));
        CommonService.buildDateMinMaxFilter(filter.getCreatedDateMin(), filter.getCreatedDateMax())
            .ifPresent(criteria::setCreatedDate);
        CommonService
            .buildDateMinMaxFilter(filter.getLastModifiedDateMin(), filter.getLastModifiedDateMax())
            .ifPresent(criteria::setLastModifiedDate);
        return criteria;
    }

    protected Specification<User> createSpecification(UserCriteria criteria) {
        Specification<User> specification = Specification.where(null);
        if (criteria != null) {

            if (criteria.getStatuses() != null) {
                assert specification != null;
                specification = specification.and(
                    buildSpecification(criteria.getStatuses(), User_.status));
            }
            if (criteria.getRoleTypes() != null) {
                assert specification != null;
                specification = specification.and(
                    buildSpecification(criteria.getRoleTypes(), User_.role));
            }
            if (criteria.getPhoneNumber() != null) {
                specification = specification.and(buildSpecification(criteria.getPhoneNumber(),
                    User_.phoneNumber));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(),
                    User_.createdDate));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(
                    buildRangeSpecification(criteria.getLastModifiedDate(),
                        User_.lastModifiedDate));
            }
            if (criteria.getQuery() != null) {
                specification = specification.and(createQuerySpecification(criteria.getQuery()));
            }
        }
        return specification;
    }

    private Specification<User> createQuerySpecification(String query) {
        if (StringUtils.isBlank(query)) {
            return null;
        }
        query = query.trim();
        return Specification
            .where(likeSpecification(root -> root.get(User_.fullName), query))
            .or(likeSpecification(root -> root.get(User_.login), query))
            .or(likeSpecification(root -> root.get(User_.email), query))
            .or(likeSpecification(root -> root.get(User_.phoneNumber), query));
    }

    protected Specification<User> likeSpecification(
        Function<Root<User>, Expression<String>> rootExpressionFunction,
        final String value) {
        return (root, query, builder) -> builder
            .like(rootExpressionFunction.apply(root), "%" + value + "%");
    }
}
