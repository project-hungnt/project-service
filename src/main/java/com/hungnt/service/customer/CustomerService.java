package com.hungnt.service.customer;

import com.hungnt.domain.customer.CustomerDomain;
import com.hungnt.domain.customer.CustomerDomain_;
import com.hungnt.web.rest.errors.FormValidateException;
import io.github.jhipster.service.filter.BigDecimalFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import java.math.BigDecimal;
import java.util.Optional;
import org.apache.commons.collections4.CollectionUtils;
import com.hungnt.repository.customer.CustomerRepository;
import com.hungnt.service.common.CommonService;
import com.hungnt.service.dto.filter.customer.CustomerCriteria;
import com.hungnt.web.rest.vm.filter.CustomerFilter;
import io.github.jhipster.service.QueryService;
import io.github.jhipster.service.filter.StringFilter;
import java.util.function.Function;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
public class CustomerService extends QueryService<CustomerDomain> {
    private final CustomerRepository customerRepository;

    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }
    @Transactional(readOnly = true)
    public Page<CustomerDomain> findByCriteria(CustomerFilter filter, Pageable page) {
        log.info("find by filter : {}, page: {}", filter, page);
        final Specification<CustomerDomain> specification = createSpecification(buildCriteria(filter));
        return customerRepository.findAll(specification, page);
    }
    @Transactional
    public CustomerDomain save(CustomerDomain customerDomain){
        return customerRepository.save(customerDomain);
    }
    public CustomerCriteria buildCriteria(CustomerFilter filter) {
        CustomerCriteria criteria = new CustomerCriteria();
        if (CollectionUtils.isNotEmpty(filter.getCities())) {
            StringFilter stringFilter = new StringFilter();
            stringFilter.setIn(filter.getCities());
            criteria.setCities(stringFilter);
        }
        criteria.setQuery(CommonService.builderQuery(filter.getQuery()));
        CommonService.buildDateMinMaxFilter(filter.getCreatedDateMin(), filter.getCreatedDateMax()).ifPresent(criteria::setCreatedDate);
        CommonService.buildDateMinMaxFilter(filter.getLastModifiedDateMin(), filter.getLastModifiedDateMax()).ifPresent(criteria::setLastModifiedDate);
        buildOrderMinMaxFilter(filter.getOrderNumberMin(),filter.getOrderNumberMax()).ifPresent(criteria::setTotalOrders);
        buildSaleMinMaxFilter(filter.getSaleMin(),filter.getSaleMax()).ifPresent(criteria::setTotalSales);
        return criteria;
    }
    protected Specification<CustomerDomain> createSpecification(CustomerCriteria criteria) {
        Specification<CustomerDomain> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getCities() !=null){
                assert specification != null;
                specification = specification.and(buildSpecification(criteria.getCities(), CustomerDomain_.cityCode));
            }
            if (criteria.getTotalOrders() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTotalOrders(), CustomerDomain_.totalOrders));
            }
            if (criteria.getTotalSales() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTotalSales(), CustomerDomain_.totalSales));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), CustomerDomain_.createdDate));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), CustomerDomain_.lastModifiedDate));
            }
            if (criteria.getQuery() != null) {
                specification = specification.and(createQuerySpecification(criteria.getQuery()));
            }
        }
        return specification;
    }
    private Specification<CustomerDomain> createQuerySpecification(String query) {
        if (StringUtils.isBlank(query)) return null;
        query = query.trim();
        return Specification.where(likeSpecification(root -> root.get(CustomerDomain_.name), query))
            .or(likeSpecification(root -> root.get(CustomerDomain_.address), query))
            .or(likeSpecification(root -> root.get(CustomerDomain_.code), query))
            .or(likeSpecification(root -> root.get(CustomerDomain_.email), query))
            .or(likeSpecification(root -> root.get(CustomerDomain_.phone), query));
    }

    protected Specification<CustomerDomain> likeSpecification(
        Function<Root<CustomerDomain>, Expression<String>> rootExpressionFunction,
        final String value) {
        return (root, query, builder) -> builder.like(rootExpressionFunction.apply(root), "%" + value + "%");
    }

    private Optional<IntegerFilter> buildOrderMinMaxFilter(Integer numberOrderMin, Integer numberOrderMax){
        if (numberOrderMin != null || numberOrderMax != null) {
            IntegerFilter integerFilter = new IntegerFilter();
            if (numberOrderMin != null) {
                integerFilter.setGreaterThanOrEqual(numberOrderMin);
            }
            if (numberOrderMax != null) {
                integerFilter.setLessThanOrEqual(numberOrderMax);
            }
            return Optional.of(integerFilter);
        } else {
            return Optional.empty();
        }
    }
    private Optional<BigDecimalFilter> buildSaleMinMaxFilter(BigDecimal saleMin, BigDecimal saleMax){
        if (saleMin != null || saleMax != null) {
            BigDecimalFilter bigDecimalFilter = new BigDecimalFilter();
            if (saleMin != null) {
                bigDecimalFilter.setGreaterThanOrEqual(saleMin);
            }
            if (saleMax != null) {
                bigDecimalFilter.setLessThanOrEqual(saleMax);
            }
            return Optional.of(bigDecimalFilter);
        } else {
            return Optional.empty();
        }
    }
    public CustomerDomain findById(Long id){
        Optional<CustomerDomain> customerDomainOptional = customerRepository.findById(id);
        if(customerDomainOptional.isEmpty()){
            throw new FormValidateException("error","Không tìm thấy khách hàng");
        }
        return customerDomainOptional.get();
    }
}
