package com.hungnt.service.qr;

import com.hungnt.config.Constants;
import com.hungnt.domain.qr.QrDomain;
import com.hungnt.repository.qr.QrRepository;
import com.hungnt.utils.RequestUtils;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
public class QrService {
    private final QrRepository qrRepository;

    public QrService(QrRepository qrRepository) {
        this.qrRepository = qrRepository;
    }

    public QrDomain generateQr(QrDomain qrDomain){
        Map<String,Object> params = new HashMap<>();
        params.put("amount",qrDomain.getAmount());
        params.put("addInfo",qrDomain.getDescription());
        params.put("accountName",qrDomain.getAccountName());
        String param = RequestUtils.urlEncodeUTF8(params);
        String subUrl = StringUtils.join(new String[] {qrDomain.getBankId(),qrDomain.getAccountNumber(),qrDomain.getTemplate()},"-");
        String link = Constants.VIET_QR+"/image/"+subUrl+".jpg?"+param;
        qrDomain.setLink(link);
        return qrRepository.save(qrDomain);
    }

}
