package com.hungnt.service.qr;

import com.hungnt.domain.qr.BankDomain;
import com.hungnt.domain.qr.BankDomain_;
import com.hungnt.repository.qr.BankRepository;
import com.hungnt.service.common.CommonService;
import com.hungnt.service.dto.filter.bank.BankCriteria;
import com.hungnt.web.rest.BankResource;
import com.hungnt.web.rest.vm.filter.BankFilter;
import io.github.jhipster.service.QueryService;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class BankService extends QueryService<BankDomain> {
    Logger log = LoggerFactory.getLogger(BankResource.class);
    private final BankRepository bankRepository;
    private BankCriteria criteria;
    private Pageable page;

    public BankService(BankRepository bankRepository) {
        this.bankRepository = bankRepository;
    }

    @Transactional(readOnly = true)
    public List<BankDomain> findByCriteria(BankCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<BankDomain> specification = createSpecification(criteria);
        List<BankDomain> bankDomains = bankRepository.findAll(specification);
        return Optional.of(bankDomains).orElseGet(ArrayList::new)
            .stream()
            .filter(Objects::nonNull)
            .collect(Collectors.toList());

    }

    @Transactional(readOnly = true)
    public Page<BankDomain> findByCriteria(BankCriteria criteria, Pageable page) {
        this.criteria = criteria;
        this.page = page;
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<BankDomain> specification = createSpecification(criteria);
        Page<BankDomain> domainPage = bankRepository.findAll(specification, page);
        return domainPage;
    }

    @Transactional(readOnly = true)
    public long countByCriteria(BankCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<BankDomain> specification = createSpecification(criteria);
        return bankRepository.count(specification);
    }
    public BankCriteria buildCriteria(BankFilter filter) {
        BankCriteria bankCriteria = new BankCriteria();
        if (StringUtils.isNotBlank(filter.getQuery())) {
            bankCriteria.setQuery(CommonService.builderQuery(filter.getQuery()));
        }
        CommonService.buildDateMinMaxFilter(filter.getCreatedDateMin(), filter.getCreatedDateMax()).ifPresent(bankCriteria::setCreatedDate);
        CommonService.buildDateMinMaxFilter(filter.getLastModifiedDateMin(), filter.getLastModifiedDateMax()).ifPresent(bankCriteria::setLastModifiedDate);
        return bankCriteria;
    }
    protected Specification<BankDomain> createSpecification(BankCriteria criteria) {
        Specification<BankDomain> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), BankDomain_.createdDate));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), BankDomain_.lastModifiedDate));
            }
            if (criteria.getQuery() != null) {
                specification = specification.and(createQuerySpecification(criteria.getQuery()));
            }
        }
        return specification;
    }
    private Specification<BankDomain> createQuerySpecification(String query) {
        if (StringUtils.isBlank(query)) return null;
        query = query.trim();
        return Specification.where(likeSpecification(root -> root.get(BankDomain_.name), query))
            .or(likeSpecification(root -> root.get(BankDomain_.shortName), query))
            .or(likeSpecification(root -> root.get(BankDomain_.code), query))
            .or(likeSpecification(root -> root.get(BankDomain_.name), query));
    }

    protected Specification<BankDomain> likeSpecification(
        Function<Root<BankDomain>, Expression<String>> metaclassFunction,
        final String value) {
        return (root, query, builder) -> builder.like(metaclassFunction.apply(root), "%" + value + "%");
    }
}
