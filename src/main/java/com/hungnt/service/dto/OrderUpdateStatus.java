package com.hungnt.service.dto;

import com.fasterxml.jackson.annotation.JsonRootName;
import java.math.BigDecimal;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@JsonRootName("order")
public class OrderUpdateStatus {
    @NotNull
    private Long id;
    @NotBlank
    private String status;
    private BigDecimal amount=BigDecimal.valueOf(0);
}
