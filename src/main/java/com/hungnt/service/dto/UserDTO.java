package com.hungnt.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hungnt.config.Constants;

import com.hungnt.domain.Authority;
import com.hungnt.domain.User;

import javax.persistence.Column;
import javax.validation.constraints.*;
import java.time.Instant;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * A DTO representing a user, with his authorities.
 */
@Data
public class UserDTO {

    private Long id;

    @NotBlank
    @Pattern(regexp = Constants.LOGIN_REGEX)
    @Size(min = 1, max = 50)
    private String login;
    @Size(min = 1, max = 50)
    private String code;
    @Size(max = 50)
    private String fullName;
    @Size(max = 50)
    private String phoneNumber;

    @NotNull
    @Size(min = 8, max = 60)
    private String password;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Email
    @Size(min = 5, max = 254)
    private String email;

    @Size(max = 256)
    private String imageUrl;

    private String status = Constants.USER_ACTIVE;

    @Size(min = 2, max = 10)
    private String langKey;

    private String createdBy;

    private Instant createdDate;

    private String lastModifiedBy;

    private Instant lastModifiedDate;

    private Set<String> authorities;
    private String role;
    public UserDTO() {
        // Empty constructor needed for Jackson.
    }

    public UserDTO(User user) {
        this.id = user.getId();
        this.login = user.getLogin();
        this.fullName = user.getFullName();
        this.phoneNumber = user.getPhoneNumber();
        this.email = user.getEmail();
        this.status = user.getStatus();
        this.imageUrl = user.getImageUrl();
        this.langKey = user.getLangKey();
        this.createdBy = user.getCreatedBy();
        this.role=user.getRole();
        this.password=user.getPassword();
        this.createdDate = user.getCreatedDate();
        this.lastModifiedBy = user.getLastModifiedBy();
        this.lastModifiedDate = user.getLastModifiedDate();
        this.authorities = user.getAuthorities().stream()
            .map(Authority::getName)
            .collect(Collectors.toSet());
    }
    public static UserDTO reconstruct(User user) {
        if (user == null || user.getId() == null) return null;
        UserDTO userDTO = new UserDTO();
        userDTO.id = user.getId();
        userDTO.code=user.getCode();
        userDTO.status = user.getStatus();
        if(user.isHasAu()){
            userDTO.authorities = user.getAuthorities().stream()
                .map(Authority::getName)
                .collect(Collectors.toSet());
        }
        userDTO.email = user.getEmail();
        userDTO.password=user.getPassword();
        userDTO.role=user.getRole();
        userDTO.phoneNumber = user.getPhoneNumber();
        userDTO.fullName = user.getFullName();
        userDTO.login = user.getLogin();
        userDTO.phoneNumber = user.getPhoneNumber();
        userDTO.lastModifiedDate = user.getLastModifiedDate();
        return userDTO;
    }
    @Override
    public String toString() {
        return "UserDTO{" +
            "login='" + login + '\'' +
            ", firstName='" + fullName + '\'' +
            ", email='" + email + '\'' +
            ", imageUrl='" + imageUrl + '\'' +
            ", activated=" + status +
            ", langKey='" + langKey + '\'' +
            ", createdBy=" + createdBy +
            ", createdDate=" + createdDate +
            ", lastModifiedBy='" + lastModifiedBy + '\'' +
            ", lastModifiedDate=" + lastModifiedDate +
            ", authorities=" + authorities +
            "}";
    }
}
