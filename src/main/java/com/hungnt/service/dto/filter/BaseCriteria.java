package com.hungnt.service.dto.filter;

import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.InstantFilter;
import io.github.jhipster.service.filter.LongFilter;
import java.io.Serializable;

public class BaseCriteria implements Serializable, Criteria {
    private static final long serialVersionUID = 1L;

    protected LongFilter id;
    protected InstantFilter createdDate;
    protected InstantFilter lastModifiedDate;
    protected String query;

    public BaseCriteria() {
    }

    public BaseCriteria(BaseCriteria other) {
        this.id = other.id==null?null:other.id.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.lastModifiedDate = other.lastModifiedDate == null ? null : other.lastModifiedDate.copy();
        this.query = other.getQuery();
    }

    @Override
    public Criteria copy() {
        return this;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public InstantFilter getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(InstantFilter lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}
