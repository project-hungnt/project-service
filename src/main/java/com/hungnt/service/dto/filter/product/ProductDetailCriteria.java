package com.hungnt.service.dto.filter.product;

import com.hungnt.service.dto.filter.BaseCriteria;
import io.github.jhipster.service.filter.BigDecimalFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class ProductDetailCriteria extends BaseCriteria {
    private StringFilter cities;
    private StringFilter productName;
    private BigDecimalFilter prices;
    private BigDecimalFilter priceSells;
    private IntegerFilter quantities;
    private IntegerFilter quantitySells;
    private LongFilter locationIds;
    private LongFilter categoryIds;
    private StringFilter barcodes;
    private StringFilter status;
}
