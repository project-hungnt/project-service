package com.hungnt.service.dto.filter;

import io.github.jhipster.service.Criteria;
import java.io.Serializable;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class BasicCriteria implements Serializable, Criteria {
    private static final long serialVersionUID = 1L;
    private String query;

    @Override
    public Criteria copy() {
        return this;
    }
}
