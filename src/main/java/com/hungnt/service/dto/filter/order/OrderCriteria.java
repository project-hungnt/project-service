package com.hungnt.service.dto.filter.order;

import com.hungnt.service.dto.filter.BaseCriteria;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class OrderCriteria extends BaseCriteria {
    private LongFilter customerIds;
    private LongFilter userIds;
    private StringFilter statuses;
    private StringFilter paymentMethods;

}

