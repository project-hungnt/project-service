package com.hungnt.service.dto.filter;

import io.github.jhipster.service.filter.StringFilter;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class SupplierCriteria extends BaseCriteria {
    private StringFilter statuses;
    private StringFilter cities;
}
