package com.hungnt.service.dto.filter.bank;

import com.hungnt.service.dto.filter.BaseCriteria;
import io.github.jhipster.service.Criteria;

public class BankCriteria extends BaseCriteria {
//    private StringFilter code;
//    private StringFilter name;
//    private StringFilter shortName;

    public BankCriteria() {
    }
    public BankCriteria(BankCriteria other) {
        this.id = other.id==null?null:other.id.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.lastModifiedDate = other.lastModifiedDate == null ? null : other.lastModifiedDate.copy();
//        this.code = other.code==null?null:other.code.copy();
//        this.name = other.name==null?null:other.name.copy();
//        this.shortName = other.shortName==null?null:other.shortName.copy();
        this.query = other.getQuery();
    }

//    public StringFilter getCode() {
//        return code;
//    }
//
//    public void setCode(StringFilter code) {
//        this.code = code;
//    }
//
//    public StringFilter getName() {
//        return name;
//    }
//
//    public void setName(StringFilter name) {
//        this.name = name;
//    }
//
//    public StringFilter getShortName() {
//        return shortName;
//    }
//
//    public void setShortName(StringFilter shortName) {
//        this.shortName = shortName;
//    }

    @Override
    public Criteria copy() {
        return this;
    }
}
