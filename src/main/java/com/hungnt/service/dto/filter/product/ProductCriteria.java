package com.hungnt.service.dto.filter.product;

import com.hungnt.service.dto.filter.BaseCriteria;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class ProductCriteria extends BaseCriteria {
    private LongFilter locationIds;
    private LongFilter categoryIds;
    private StringFilter status;
}
