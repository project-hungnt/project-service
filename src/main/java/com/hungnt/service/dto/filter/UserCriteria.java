package com.hungnt.service.dto.filter;

import io.github.jhipster.service.filter.StringFilter;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class UserCriteria  extends BaseCriteria {
    private StringFilter statuses;
    private StringFilter roleTypes;
    private StringFilter phoneNumber;
}
