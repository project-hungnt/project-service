package com.hungnt.service.dto.filter.customer;

import com.hungnt.service.dto.filter.BaseCriteria;
import io.github.jhipster.service.filter.BigDecimalFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.StringFilter;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class CustomerCriteria extends BaseCriteria {
    private StringFilter cities;
    private BigDecimalFilter totalSales;
    private IntegerFilter totalOrders;

}
