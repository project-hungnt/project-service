package com.hungnt.service;

import com.hungnt.domain.CityDomain;
import com.hungnt.domain.CityDomain_;
import com.hungnt.repository.CityRepository;
import com.hungnt.service.common.CommonService;
import com.hungnt.service.dto.filter.BasicCriteria;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
public class CityService {
    private final CityRepository cityRepository;

    public CityService(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }
    public List<CityDomain> findAll(){
        List<CityDomain> result = cityRepository.findAll();
        return Optional.of(result).orElseGet(ArrayList::new)
            .stream().filter(Objects::nonNull)
            .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<CityDomain> findByCriteria(BasicCriteria criteria) {
        log.debug("find by criteria : {}, page: {}", criteria);
        final Specification<CityDomain> specification = createSpecification(criteria);
        List<CityDomain> result = cityRepository.findAll(specification);
        return Optional.of(result).orElseGet(ArrayList::new)
            .stream().filter(Objects::nonNull)
            .collect(Collectors.toList());
    }
    @Transactional(readOnly = true)
    public Page<CityDomain> findByCriteria(BasicCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<CityDomain> specification = createSpecification(criteria);
        return cityRepository.findAll(specification, page);
    }
    public BasicCriteria buildCriteria(String query) {
        BasicCriteria bankCriteria = new BasicCriteria();
        if (StringUtils.isNotBlank(query)) {
            bankCriteria.setQuery(CommonService.builderQuery(query));
        }
        return bankCriteria;
    }
    protected Specification<CityDomain> createSpecification(BasicCriteria criteria) {
        Specification<CityDomain> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getQuery() == null) {
                return specification;
            }
            assert specification != null;
            specification = specification.and(createQuerySpecification(criteria.getQuery()));
        }
        return specification;
    }
    private Specification<CityDomain> createQuerySpecification(String query) {
        if (StringUtils.isBlank(query)) return null;
        query = query.trim();
        Specification<CityDomain> domainSpecification = Specification
            .where(likeSpecification(root -> root.get(CityDomain_.cityName), query))
            .or(likeSpecification(root -> root.get(CityDomain_.cityCode), query));
        return domainSpecification;
    }

    protected Specification<CityDomain> likeSpecification(
        Function<Root<CityDomain>, Expression<String>> expressionFunction,
        final String value) {
        return (root, query, builder) -> builder.like(expressionFunction.apply(root), "%" + value + "%");
    }
}
