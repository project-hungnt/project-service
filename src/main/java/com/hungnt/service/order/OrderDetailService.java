package com.hungnt.service.order;

import com.hungnt.domain.order.OrderDetailDomain;
import com.hungnt.domain.order.OrderDomain;
import com.hungnt.domain.product.ProductDetailDomain;
import com.hungnt.repository.order.OrderDetailRepository;
import com.hungnt.repository.product.ProductDetailRepository;
import com.hungnt.service.product.ProductDetailService;
import com.hungnt.web.rest.errors.FormValidateException;
import com.hungnt.web.rest.vm.order.OrderDetailVM;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;

@Service
public class OrderDetailService {
    private final OrderDetailRepository orderDetailRepository;
    private final ProductDetailRepository productDetailRepository;
    private final ProductDetailService productDetailService;
    public OrderDetailService(
        OrderDetailRepository orderDetailRepository,
        ProductDetailRepository productDetailRepository,
        ProductDetailService productDetailService) {
        this.orderDetailRepository = orderDetailRepository;
        this.productDetailRepository = productDetailRepository;
        this.productDetailService = productDetailService;
    }

    public void createOrderDetails (OrderDomain orderDomain,List<OrderDetailVM> detailVMS){
        List<OrderDetailDomain> orderDetailDomains = new ArrayList<>();
        if(!detailVMS.isEmpty()){
            detailVMS.forEach(item -> {
                OrderDetailDomain orderDetailDomain = new OrderDetailDomain();
                orderDetailDomain.setOrderId(orderDomain.getId());
                orderDetailDomain.setOrderCode(orderDomain.getCode());
                orderDetailDomain.setDiscount(item.getDiscount());
                orderDetailDomain.setPrice(item.getPrice());
                orderDetailDomain.setQuantity(item.getQuantity());
                if(item.getProductDetailId()!=null && item.getProductDetailId()>0){
                    Optional<ProductDetailDomain>productDetailDomain = productDetailRepository.findById(item.getProductDetailId());
                    if(productDetailDomain.isPresent()){
                        if(productDetailDomain.get().getQuantity()<item.getQuantity()){
                            throw new FormValidateException("error","Sản phẩm này đã hết hàng");
                        }
                    }else{
                        throw new FormValidateException("error","Không tìm thấy phiên bản sản phẩm");
                    }
                }else{
                    throw new FormValidateException("error","Đơn hàng chưa có sản phẩm không thể thanh toán");
                }
                orderDetailDomain.setProductDetailId(item.getProductDetailId());
                orderDetailDomains.add(orderDetailDomain);
                productDetailService.updateQuantityProductDetail(item.getProductDetailId(),item.getQuantity());
            });
        }else{
            throw new FormValidateException("error","Đơn hàng chưa có sản phẩm không thể thanh toán");
        }
        orderDetailRepository.saveAll(orderDetailDomains);
    }
    public List<OrderDetailDomain> getOrderDetailByOrderId(Long id){
        return orderDetailRepository.findAllByOrOrderId(id);
    }

}
