package com.hungnt.service.order;

import com.hungnt.config.Constants;
import com.hungnt.domain.customer.CustomerDomain;
import com.hungnt.domain.order.OrderDomain;
import com.hungnt.domain.order.OrderDomain_;
import com.hungnt.repository.customer.CustomerRepository;
import com.hungnt.repository.order.OrderRepository;
import com.hungnt.service.common.CommonService;
import com.hungnt.service.dto.OrderUpdateStatus;
import com.hungnt.service.dto.filter.order.OrderCriteria;
import com.hungnt.utils.TextUtils;
import com.hungnt.web.rest.errors.FormValidateException;
import com.hungnt.web.rest.vm.filter.OrderFilter;
import com.hungnt.web.rest.vm.order.OrderVM;
import io.github.jhipster.service.QueryService;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import java.math.BigDecimal;
import java.util.Optional;
import java.util.function.Function;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
@Slf4j
public class OrderService extends QueryService<OrderDomain> {

    private final OrderRepository orderRepository;
    private final CustomerRepository customerRepository;
    private final OrderDetailService orderDetailService;

    public OrderService(OrderRepository orderRepository,
        CustomerRepository customerRepository,
        OrderDetailService orderDetailService) {
        this.orderRepository = orderRepository;
        this.customerRepository = customerRepository;
        this.orderDetailService = orderDetailService;
    }
    @Transactional(readOnly = true)
    public Page<OrderDomain> findByCriteria(OrderFilter filter, Pageable page) {
        log.info("find by filter : {}, page: {}", filter, page);
        final Specification<OrderDomain> specification = createSpecification(
            buildCriteria(filter));
        return orderRepository.findAll(specification, page);
    }
    public OrderCriteria buildCriteria(OrderFilter filter) {
        OrderCriteria criteria = new OrderCriteria();
        if (CollectionUtils.isNotEmpty(filter.getCustomers())) {
            LongFilter stringFilter = new LongFilter();
            stringFilter.setIn(filter.getCustomers());
            criteria.setCustomerIds(stringFilter);
        }

        if (CollectionUtils.isNotEmpty(filter.getStatuses())) {
            StringFilter statusFilter = new StringFilter();
            statusFilter.setIn(filter.getStatuses());
            criteria.setStatuses(statusFilter);
        }
        if (CollectionUtils.isNotEmpty(filter.getPaymentMethods())) {
            StringFilter statusFilter = new StringFilter();
            statusFilter.setIn(filter.getPaymentMethods());
            criteria.setPaymentMethods(statusFilter);
        }
        if (CollectionUtils.isNotEmpty(filter.getUsers())) {
            LongFilter stringFilter = new LongFilter();
            stringFilter.setIn(filter.getUsers());
            criteria.setUserIds(stringFilter);
        }
        criteria.setQuery(CommonService.builderQuery(filter.getQuery()));
        CommonService.buildDateMinMaxFilter(filter.getCreatedDateMin(), filter.getCreatedDateMax())
            .ifPresent(criteria::setCreatedDate);
        CommonService
            .buildDateMinMaxFilter(filter.getLastModifiedDateMin(), filter.getLastModifiedDateMax())
            .ifPresent(criteria::setLastModifiedDate);
        return criteria;
    }

    protected Specification<OrderDomain> createSpecification(OrderCriteria criteria) {
        Specification<OrderDomain> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getCustomerIds() != null) {
                assert specification != null;
                specification = specification.and(
                    buildSpecification(criteria.getCustomerIds(), OrderDomain_.customerId));
            }
            if (criteria.getUserIds() != null) {
                assert specification != null;
                specification = specification.and(
                    buildSpecification(criteria.getUserIds(), OrderDomain_.userId));
            }
            if (criteria.getStatuses() != null) {
                assert specification != null;
                specification = specification.and(
                    buildSpecification(criteria.getStatuses(), OrderDomain_.status));
            }
            if (criteria.getPaymentMethods() != null) {
                assert specification != null;
                specification = specification.and(
                    buildSpecification(criteria.getPaymentMethods(), OrderDomain_.paymentMethod));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(),
                    OrderDomain_.createdDate));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(
                    buildRangeSpecification(criteria.getLastModifiedDate(),
                        OrderDomain_.lastModifiedDate));
            }
            if (criteria.getQuery() != null) {
                specification = specification.and(createQuerySpecification(criteria.getQuery()));
            }
        }
        return specification;
    }

    private Specification<OrderDomain> createQuerySpecification(String query) {
        if (StringUtils.isBlank(query)) {
            return null;
        }
        query = query.trim();
        return Specification
            .where(likeSpecification(root -> root.get(OrderDomain_.code), query))
            .or(likeSpecification(root -> root.get(OrderDomain_.note), query));
    }

    protected Specification<OrderDomain> likeSpecification(
        Function<Root<OrderDomain>, Expression<String>> rootExpressionFunction,
        final String value) {
        return (root, query, builder) -> builder
            .like(rootExpressionFunction.apply(root), "%" + value + "%");
    }
    @Transactional
    public OrderDomain createOrder(OrderVM orderVM) {
        try {
            OrderDomain orderDomain = buildOrderDomain(orderVM);
            orderDomain = orderRepository.save(orderDomain);
            orderDetailService.createOrderDetails(orderDomain,orderVM.getOrderDetails());
            return orderDomain;
        } catch (Exception e) {
            log.info("create order errorL {}", e.getMessage());
            throw new FormValidateException("error", e.getMessage());
        }

    }

    private OrderDomain buildOrderDomain(OrderVM orderVM) {
        OrderDomain orderDomain = new OrderDomain();
        orderDomain.setCode(TextUtils.getCode("SON",orderRepository.count()));
        orderDomain.setUserId(orderVM.getUserId());
        CustomerDomain customerDomain;
        if(orderVM.getCustomerId()!=null && orderVM.getCustomerId()>0){
            Optional<CustomerDomain> customerDomainOptional = customerRepository.findById(orderVM.getCustomerId());
            if(customerDomainOptional.isEmpty()){
                throw new FormValidateException("error","Không tim thấy khách hàng");
            }
            customerDomain=customerDomainOptional.get();
        }else{
            customerDomain=customerRepository.findById(Long.valueOf(1)).get();
        }
        if(orderVM.getStatus().equals(Constants.ORDER_PAID)){
            customerDomain.setTotalOrders(customerDomain.getTotalOrders()+1);
            BigDecimal sales = customerDomain.getTotalSales().add(orderVM.getTotalPrice());
            customerDomain.setTotalSales(sales);
            customerRepository.save(customerDomain);
        }
        orderDomain.setCustomerId(customerDomain.getId());
        orderDomain.setDiscount(orderVM.getDiscount());
        orderDomain.setMoney(orderVM.getMoney());
        orderDomain.setPaymentMethod(orderVM.getPaymentMethod());
        orderDomain.setTotalPrice(orderVM.getTotalPrice());
        orderDomain.setTotalQuantity(orderVM.getTotalQuantity());
        orderDomain.setStatus(orderVM.getStatus());
        orderDomain.setNote(orderVM.getNote());
        return orderDomain;
    }
    public OrderDomain findById(Long id){
        Optional<OrderDomain> orderDomain = orderRepository.findById(id);
        if(orderDomain.isEmpty()){
            throw new FormValidateException("error","Không tìm thấy đơn hàng");
        }
        return orderDomain.get();
    }
    @Transactional
    public OrderDomain updateStatus(OrderUpdateStatus orderUpdateStatus){
        OrderDomain orderDomain = findById(orderUpdateStatus.getId());
        CustomerDomain customerDomain;
        orderDomain.setMoney(orderUpdateStatus.getAmount());
        if(orderDomain.getCustomerId()!=null && orderDomain.getCustomerId()>0){
            Optional<CustomerDomain> customerDomainOptional = customerRepository.findById(orderDomain.getCustomerId());
            if(customerDomainOptional.isEmpty()){
                throw new FormValidateException("error","Không tim thấy khách hàng");
            }
            customerDomain=customerDomainOptional.get();
        }else{
            customerDomain=customerRepository.findById(Long.valueOf(1)).get();
        }
        if(orderUpdateStatus.getStatus().equals(Constants.ORDER_PAID)){
            customerDomain.setTotalOrders(customerDomain.getTotalOrders()+1);
            BigDecimal sales = customerDomain.getTotalSales().add(orderDomain.getTotalPrice());
            customerDomain.setTotalSales(sales);
            customerRepository.save(customerDomain);
        }
        orderDomain.updateStatus(orderUpdateStatus.getStatus());
       return orderRepository.save(orderDomain);
    }
    public OrderDomain findByCode(String code){
        Optional<OrderDomain> orderDomain = orderRepository.findByCode(code);
        if(orderDomain.isEmpty()){
            throw new FormValidateException("error","Không tìm thấy đơn hàng");
        }
        return orderDomain.get();
    }
}
