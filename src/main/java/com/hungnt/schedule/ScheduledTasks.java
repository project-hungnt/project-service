package com.hungnt.schedule;

import com.hungnt.clients.VietQrClient;
import com.hungnt.domain.qr.BankDomain;
import com.hungnt.repository.qr.BankRepository;
import com.hungnt.web.rest.vm.qr.ListBankVM;
import java.util.Optional;
import ma.glasnost.orika.MapperFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTasks {

    private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);
    private final MapperFacade mapper;
    private final VietQrClient qrClient;
    private final BankRepository bankRepository;

    public ScheduledTasks(MapperFacade mapper, VietQrClient qrClient,
        BankRepository bankRepository) {
        this.mapper = mapper;
        this.qrClient = qrClient;
        this.bankRepository = bankRepository;
    }

    @Scheduled(cron = "0 0 * * 7 *")
    public void cloneBankFromVietQr() {
        log.info("start clone job");
        try {
            ListBankVM result = qrClient.getBanks();
            if (result != null && !result.getData().isEmpty()){
               result.getData().forEach(bankVM->{
                   Optional<BankDomain> bankDomainOptional = bankRepository.findByCode(bankVM.getCode());
                   BankDomain domain;
                   if(bankDomainOptional.isEmpty()){
                       log.info("clone job add bank {}",bankVM.getCode());
                       domain=mapper.map(bankVM,BankDomain.class);
                   }else {
                       log.info("clone job update bank {}",bankVM.getCode());
                       domain=bankDomainOptional.get().update(bankVM);
                   }
                   bankRepository.save(domain);
               });
            }
        } catch (Exception e) {
            log.info(e.getMessage());
        }
    }
}
