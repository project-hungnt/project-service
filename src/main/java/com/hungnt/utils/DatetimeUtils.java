package com.hungnt.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.Date;
import java.util.stream.Stream;
import org.apache.commons.lang3.StringUtils;

public class DatetimeUtils {

    public DatetimeUtils() {
    }

    public static String convertInstantToString(Instant instant, String format) {
        if (instant == null || StringUtils.isBlank(format)) {
            return "";
        }
        DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(format);
        return DATE_TIME_FORMATTER.format(instant);
    }

    public static String convertLocalDateToString(LocalDate localDate, String format) {
        if (localDate == null || StringUtils.isBlank(format)) {
            return "";
        }
        DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(format);
        return DATE_TIME_FORMATTER.format(localDate);
    }

    public static LocalDate max(LocalDate... dates) {
        return Stream.of(dates)
            .max(Comparator.comparing(date -> date))
            .orElse(null);
    }

    public static LocalDateTime convertLocalDateTimeToLocal(Instant instant) {
        return instant == null ? null
            : LocalDateTime.ofInstant(instant, ZoneId.of("Asia/Ho_Chi_Minh"));
    }

    public static String convertLocalDateTimeToString(LocalDateTime localDateTime, String format) {
        if (localDateTime == null) {
            return "";
        }
        DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(format);
        return DATE_TIME_FORMATTER.format(localDateTime);
    }

    public static String convertDateToString(Date date, String format) {
        if (date == null) {
            return "";
        }
        DateFormat DATE_TIME_FORMATTER = new SimpleDateFormat(format);
        return DATE_TIME_FORMATTER.format(date);
    }
}
