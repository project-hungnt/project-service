package com.hungnt.utils;

import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.util.CollectionUtils;

public class Utils {
    public Utils() {
    }

    public static String md5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuilder sb = new StringBuilder();
            for (byte b : array) {
                sb.append(Integer.toHexString((b & 0xFF) | 0x100), 1, 3);
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException ignored) {
        }
        return null;
    }

    public static Collection<String> normalize(Collection<?> collection) {
        if (collection == null) {
            return Collections.emptyList();
        }
        return collection.stream()
            .filter(Objects::nonNull)
            .map(String::valueOf)
            .collect(Collectors.toList());
    }

    public static <T> String joinList(Collection<T> list, String delimiter) {
        if (CollectionUtils.isEmpty(list)) {
            return "";
        } else {
            if (delimiter == null) {
                delimiter = ",";
            }

            return list.stream().map(Object::toString).collect(Collectors.joining(delimiter));
        }
    }

    public static <T> String joinList(Collection<T> list) {
        return joinList(list, ",");
    }
}
