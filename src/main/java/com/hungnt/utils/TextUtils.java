package com.hungnt.utils;

import com.google.common.base.CaseFormat;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;

public class TextUtils {
    private TextUtils() {
    }

    public static String generatePropertyCode(String input) {
        if (StringUtils.isBlank(input)) {
            return "";
        } else {
            String code = removeVietnameseChar(input);
            code = replaceSpecialWithUnderScore(code);
            return StringUtils.upperCase(code);
        }
    }

    private static String replaceSpecialWithUnderScore(String code) {
        if (StringUtils.isBlank(code)) {
            return code;
        } else if (Pattern.compile("[^A-Za-z0-9_]").matcher(code).find()) {
            return replaceSpecialWithUnderScore(code.replaceAll("[^A-Za-z0-9_]", "_"));
        } else {
            return Pattern.compile("_{2}").matcher(code).find() ? replaceSpecialWithUnderScore(code.replaceAll("_+", "_")) : code;
        }
    }

    public static String removeVietnameseChar(String input) {
        input = input.replaceAll("[àáạảãâầấậẩẫăằắặẳẵ]", "a");
        input = input.replaceAll("[èéẹẻẽêềếệểễ]", "e");
        input = input.replaceAll("[ìíịỉĩ]", "i");
        input = input.replaceAll("[òóọỏõôồốộổỗơờớợởỡ]", "o");
        input = input.replaceAll("[ùúụủũưừứựửữ]", "u");
        input = input.replaceAll("[ỳýỵỷỹ]", "y");
        input = input.replaceAll("[đ]", "d");
        input = input.replaceAll("[ÀÁẠẢÃÂẦẤẬẨẪĂẰẮẶẲẴ]", "A");
        input = input.replaceAll("[ÈÉẸẺẼÊỀẾỆỂỄ]", "E");
        input = input.replaceAll("[ÌÍỊỈĨ]", "I");
        input = input.replaceAll("[ÒÓỌỎÕÔỒỐỘỔỖƠỜỚỢỞỠ]", "O");
        input = input.replaceAll("[ÙÚỤỦŨƯỪỨỰỬỮ]", "U");
        input = input.replaceAll("[ỲÝỴỶỸ]", "Y");
        input = input.replaceAll("[Đ]", "D");
        return input;
    }

    public static String toSnakeCase(String input) {
        return StringUtils.isEmpty(input) ? input : CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, input);
    }
    public static String getCode(String prefix, Long count) {
        String suffix = String.format("%05d", count+1);
        return prefix.toUpperCase() + suffix;
    }
}
