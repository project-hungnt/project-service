package com.hungnt.utils;

import com.hungnt.config.Constants;
import com.hungnt.filter.ResettableStreamHttpServletRequest;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public class RequestUtils {
    private RequestUtils() {
    }

    public static String getBody(HttpServletRequest request) throws IOException {
        ResettableStreamHttpServletRequest wrappedRequest = (ResettableStreamHttpServletRequest) request;
        wrappedRequest.resetInputStream();
        return wrappedRequest.getReader().lines().reduce("", (accumulator, actual) -> accumulator + actual);
    }

    public static Pageable limitPaging(Pageable pageable, Sort defaultSort) {
        Sort sort = pageable.getSortOr(defaultSort);
        int pageSize = pageable.getPageSize();
        return PageRequest.of(pageable.getPageNumber(), pageSize, sort);
    }
    static String urlEncodeUTF8(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new UnsupportedOperationException(e);
        }
    }
    public static String urlEncodeUTF8(Map<?,?> map){
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<?,?> entry : map.entrySet()) {
            if (sb.length() > 0) {
                sb.append("&");
            }
            sb.append(String.format("%s=%s",
                urlEncodeUTF8(entry.getKey().toString()),
                urlEncodeUTF8(entry.getValue().toString())
            ));
        }
        return sb.toString();
    }
}
