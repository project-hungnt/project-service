package com.hungnt.utils;

import com.google.common.collect.ImmutableList;
import com.hungnt.web.rest.errors.FormValidateException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.util.List;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;

public class FileUtil {
    public static final String FILE_EXTENSION_XLSX = "xlsx";
    public static final String FILE_EXTENSION_XLS = "xls";
    public static final String FILE_EXTENSION_PDF = "pdf";
    public static final String FILE_EXTENSION_PNG = "png";
    public static final String FILE_EXTENSION_JPG = "jpg";
    public static final String FILE_EXTENSION_RAR = "rar";
    public static final String FILE_EXTENSION_ZIP = "zip";
    public static final String FILE_EXTENSION_JPEG = "jpeg";

    public static final List<String> FILE_EXTENSIONS = ImmutableList.<String>builder()
        .add(FILE_EXTENSION_XLSX)
        .add(FILE_EXTENSION_XLS)
        .add(FILE_EXTENSION_PDF)
        .add(FILE_EXTENSION_PNG)
        .add(FILE_EXTENSION_JPG)
        .add(FILE_EXTENSION_RAR)
        .add(FILE_EXTENSION_ZIP)
        .add(FILE_EXTENSION_JPEG)
        .build();

    public static final List<String> FILE_UPLOAD_EXTENSIONS = ImmutableList.<String>builder()
        .add(FILE_EXTENSION_PDF)
        .add(FILE_EXTENSION_PNG)
        .add(FILE_EXTENSION_JPG)
        .add(FILE_EXTENSION_RAR)
        .add(FILE_EXTENSION_ZIP)
        .add(FILE_EXTENSION_JPEG)
        .build();

    private final static String[] hex = { "%00", "%01", "%02", "%03", "%04", "%05",
        "%06", "%07", "%08", "%09", "%0A", "%0B", "%0C", "%0D", "%0E",
        "%0F", "%10", "%11", "%12", "%13", "%14", "%15", "%16", "%17",
        "%18", "%19", "%1A", "%1B", "%1C", "%1D", "%1E", "%1F", "%20",
        "%21", "%22", "%23", "%24", "%25", "%26", "%27", "%28", "%29",
        "%2A", "%2B", "%2C", "%2D", "%2E", "%2F", "%30", "%31", "%32",
        "%33", "%34", "%35", "%36", "%37", "%38", "%39", "%3A", "%3B",
        "%3C", "%3D", "%3E", "%3F", "%40", "%41", "%42", "%43", "%44",
        "%45", "%46", "%47", "%48", "%49", "%4A", "%4B", "%4C", "%4D",
        "%4E", "%4F", "%50", "%51", "%52", "%53", "%54", "%55", "%56",
        "%57", "%58", "%59", "%5A", "%5B", "%5C", "%5D", "%5E", "%5F",
        "%60", "%61", "%62", "%63", "%64", "%65", "%66", "%67", "%68",
        "%69", "%6A", "%6B", "%6C", "%6D", "%6E", "%6F", "%70", "%71",
        "%72", "%73", "%74", "%75", "%76", "%77", "%78", "%79", "%7A",
        "%7B", "%7C", "%7D", "%7E", "%7F", "%80", "%81", "%82", "%83",
        "%84", "%85", "%86", "%87", "%88", "%89", "%8A", "%8B", "%8C",
        "%8D", "%8E", "%8F", "%90", "%91", "%92", "%93", "%94", "%95",
        "%96", "%97", "%98", "%99", "%9A", "%9B", "%9C", "%9D", "%9E",
        "%9F", "%A0", "%A1", "%A2", "%A3", "%A4", "%A5", "%A6", "%A7",
        "%A8", "%A9", "%AA", "%AB", "%AC", "%AD", "%AE", "%AF", "%B0",
        "%B1", "%B2", "%B3", "%B4", "%B5", "%B6", "%B7", "%B8", "%B9",
        "%BA", "%BB", "%BC", "%BD", "%BE", "%BF", "%C0", "%C1", "%C2",
        "%C3", "%C4", "%C5", "%C6", "%C7", "%C8", "%C9", "%CA", "%CB",
        "%CC", "%CD", "%CE", "%CF", "%D0", "%D1", "%D2", "%D3", "%D4",
        "%D5", "%D6", "%D7", "%D8", "%D9", "%DA", "%DB", "%DC", "%DD",
        "%DE", "%DF", "%E0", "%E1", "%E2", "%E3", "%E4", "%E5", "%E6",
        "%E7", "%E8", "%E9", "%EA", "%EB", "%EC", "%ED", "%EE", "%EF",
        "%F0", "%F1", "%F2", "%F3", "%F4", "%F5", "%F6", "%F7", "%F8",
        "%F9", "%FA", "%FB", "%FC", "%FD", "%FE", "%FF" };

    public FileUtil() {
    }

    public static byte[] base64ToByte(String base64Str) {
        return Base64.decodeBase64(base64Str);
    }

    public static BigDecimal calculateFileSizeInMB(String base64) {
        if (StringUtils.isEmpty(base64))
            return BigDecimal.ZERO;
        return BigDecimal.valueOf(base64.length() * 3).divide(BigDecimal.valueOf(4 * 1024 * 1024), 2, RoundingMode.HALF_UP);
    }

    public static BigDecimal calculateFileSizeInKB(String base64) {
        if (StringUtils.isEmpty(base64))
            return BigDecimal.ZERO;
        return BigDecimal.valueOf(base64.length() * 3).divide(BigDecimal.valueOf(4 * 1024), 2, RoundingMode.HALF_UP);
    }

    public static String getExtension(String filename) {
        if (filename == null) {
            return null;
        } else {
            int index = indexOfExtension(filename);
            String extension = index == -1 ? "" : filename.substring(index + 1);
            return StringUtils.lowerCase(extension);
        }
    }

    private static int indexOfExtension(String filename) {
        if (filename == null) {
            return -1;
        } else {
            int extensionPos = filename.lastIndexOf(46);
            int lastSeparator = indexOfLastSeparator(filename);
            return lastSeparator > extensionPos ? -1 : extensionPos;
        }
    }

    private static int indexOfLastSeparator(String filename) {
        if (filename == null) {
            return -1;
        } else {
            int lastUnixPos = filename.lastIndexOf(47);
            int lastWindowsPos = filename.lastIndexOf(92);
            return Math.max(lastUnixPos, lastWindowsPos);
        }
    }

    public static String generateFileName(String prefix, String name, String suffix, String extension) {
        String md5 = Utils.md5(DatetimeUtils.convertLocalDateTimeToLocal(Instant.now()).toString());
        if (StringUtils.isBlank(extension) || FILE_EXTENSIONS.stream().noneMatch(s -> s.equals(extension))) {
            throw new FormValidateException("file", "extension is null or not support");
        }
        return (StringUtils.isBlank(prefix) ? "" : prefix) +
            (StringUtils.isBlank(name) ? "" : name) +
            (StringUtils.isBlank(suffix) ? "" : suffix) +
            md5 +
            "." +
            extension;
    }

    public static String encodeHex(String s) {
        StringBuilder builder = new StringBuilder();
        int len = s.length();
        for (int i = 0; i < len; i++) {
            int ch = s.charAt(i);
            if ('A' <= ch && ch <= 'Z') { // 'A'..'Z'
                builder.append((char) ch);
            } else if ('a' <= ch && ch <= 'z') { // 'a'..'z'
                builder.append((char) ch);
            } else if ('0' <= ch && ch <= '9') { // '0'..'9'
                builder.append((char) ch);
            } else if (ch == ' ') { // space
                builder.append('+');
            } else if (ch == '-'
                || ch == '_' // unreserved
                || ch == '.' || ch == '!' || ch == '~' || ch == '*'
                || ch == '\'' || ch == '(' || ch == ')') {
                builder.append((char) ch);
            } else if (ch <= 0x007f) { // other ASCII
                builder.append(hex[ch]);
            } else if (ch <= 0x07FF) { // non-ASCII <= 0x7FF
                builder.append(hex[0xc0 | (ch >> 6)]);
                builder.append(hex[0x80 | (ch & 0x3F)]);
            } else { // 0x7FF < ch <= 0xFFFF
                builder.append(hex[0xe0 | (ch >> 12)]);
                builder.append(hex[0x80 | ((ch >> 6) & 0x3F)]);
                builder.append(hex[0x80 | (ch & 0x3F)]);
            }
        }
        return builder.toString();
    }
}
