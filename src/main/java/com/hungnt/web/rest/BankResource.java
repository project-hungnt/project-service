package com.hungnt.web.rest;

import com.hungnt.domain.qr.BankDomain;
import com.hungnt.service.qr.BankService;
import com.hungnt.service.dto.filter.bank.BankCriteria;
import com.hungnt.utils.RequestUtils;
import com.hungnt.web.rest.vm.common.DataCollectionVM;
import com.hungnt.web.rest.vm.filter.BankFilter;
import com.hungnt.web.rest.vm.qr.BankVM;
import lombok.val;
import ma.glasnost.orika.MapperFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class BankResource {

    Logger log = LoggerFactory.getLogger(BankResource.class);
    private final MapperFacade mapper;
    private final BankService bankService;

    public BankResource(MapperFacade mapperFacade,BankService bankService) {
        this.mapper = mapperFacade;
        this.bankService = bankService;
    }

    @GetMapping("/banks")
    public ResponseEntity<DataCollectionVM<BankVM>> getBanks(BankFilter filter,
        Pageable pageable) {
        log.info("Request To Filter And Search VMPOSVimo {}", filter.toString());
        Pageable pageRequest = RequestUtils.limitPaging(pageable, Sort.by(Direction.ASC, "id"));
        BankCriteria criteria = bankService.buildCriteria(filter);
        Page<BankDomain> bankDomains = bankService.findByCriteria(criteria, pageRequest);
        val result = new DataCollectionVM<BankVM>();
        result.setData(bankDomains.map(m -> mapper.map(m, BankVM.class)).getContent());
        result.setLimit(pageable.getPageSize());
        result.setPage(pageable.getPageNumber());
        result.setTotal(bankDomains.getTotalElements());
        return ResponseEntity.ok().body(result);
    }

}
