package com.hungnt.web.rest;

import com.hungnt.domain.product.ProductDetailDomain;
import com.hungnt.domain.product.ProductDomain;
import com.hungnt.repository.product.ProductDetailRepository;
import com.hungnt.repository.product.ProductRepository;
import com.hungnt.security.AuthoritiesConstants;
import com.hungnt.service.product.ProductDetailService;
import com.hungnt.service.product.ProductService;
import com.hungnt.web.rest.errors.FormValidateException;
import com.hungnt.web.rest.vm.common.DataCollectionVM;
import com.hungnt.web.rest.vm.filter.ProductDetailFilter;
import com.hungnt.web.rest.vm.filter.ProductFilter;
import com.hungnt.web.rest.vm.product.ProductDetailCreateRequest;
import com.hungnt.web.rest.vm.product.ProductDetailResponseVM;
import com.hungnt.web.rest.vm.product.ProductUpdateRequest;
import com.hungnt.web.rest.vm.product.UpdateDefaultImageVM;
import com.hungnt.web.rest.vm.product.UpdateStatusVM;
import com.hungnt.web.rest.vm.product.ProductRequestVM;
import com.hungnt.web.rest.vm.product.ProductResponseVM;
import java.io.IOException;
import java.util.List;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import ma.glasnost.orika.MapperFacade;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@Slf4j
public class ProductResource {

    private final ProductService productService;
    private final ProductDetailService productDetailService;
    private final MapperFacade mapperFacade;
    private final ProductRepository productRepository;
    private final ProductDetailRepository productDetailRepository;

    public ProductResource(ProductService productService,
        ProductDetailService productDetailService,
        MapperFacade mapperFacade,
        ProductRepository productRepository,
        ProductDetailRepository productDetailRepository) {
        this.productService = productService;
        this.productDetailService = productDetailService;
        this.mapperFacade = mapperFacade;
        this.productRepository = productRepository;
        this.productDetailRepository = productDetailRepository;
    }

    @PostMapping("/products")
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN + "\",\"" + AuthoritiesConstants.PRODUCT + "\")")
    public ResponseEntity<ProductResponseVM> create(
        @RequestBody @Valid ProductRequestVM productRequestVM)
        throws IOException, NotFoundException {
        return ResponseEntity
            .ok(mapperFacade.map(productService.create(productRequestVM), ProductResponseVM.class));
    }
    @GetMapping("/products")
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN + "\",\"" + AuthoritiesConstants.PRODUCT + "\")")
    public ResponseEntity<DataCollectionVM<ProductResponseVM>> getProducts(ProductFilter filter, Pageable pageable) {
        log.info("Request To Filter And Search product {}", filter.toString());
        Page<ProductDomain> productDomains = productService.findByCriteria(filter, pageable);
        List<ProductResponseVM> productResponseVMS = productDomains.map(m -> mapperFacade.map(m, ProductResponseVM.class)).getContent();
        if(!productResponseVMS.isEmpty()){
            productResponseVMS.forEach(item->{
                List<ProductDetailDomain> productDetailDomains=productDetailService.finAllByProductId(item.getId());
                item.setProductDetails(mapperFacade.mapAsList(productDetailDomains,ProductDetailResponseVM.class));
            });
        }
        val result = new DataCollectionVM<ProductResponseVM>();
        result.setData(productResponseVMS);
        result.setLimit(pageable.getPageSize());
        result.setPage(pageable.getPageNumber());
        result.setTotal(productDomains.getTotalElements());
        return ResponseEntity.ok().body(result);
    }
    @GetMapping("/products/inventories")
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN + "\",\"" + AuthoritiesConstants.PRODUCT + "\",\"" + AuthoritiesConstants.ORDER + "\")")
    public ResponseEntity<DataCollectionVM<ProductDetailResponseVM>> getInventories(
        ProductDetailFilter filter, Pageable pageable) {
        log.info("Request To Filter And Search product {}", filter.toString());
        Page<ProductDetailDomain> productDetailDomains = productDetailService
            .findByCriteria(filter, pageable);
        val result = new DataCollectionVM<ProductDetailResponseVM>();
        result.setData(
            productDetailDomains.map(m -> mapperFacade.map(m, ProductDetailResponseVM.class))
                .getContent());
        result.setLimit(pageable.getPageSize());
        result.setPage(pageable.getPageNumber());
        result.setTotal(productDetailDomains.getTotalElements());
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/products/{code}")
    public ResponseEntity<ProductResponseVM> getProduct(@PathVariable("code") String productCode) {
        try {
            log.info("find product by code {}", productCode);
            ProductDomain productDomain = productService.findByCode(productCode);
            List<ProductDetailDomain> productDetails = productDetailService.finAllByProductId(productDomain.getId());
            ProductResponseVM result = mapperFacade.map(productDomain, ProductResponseVM.class);
            result.setProductDetails(mapperFacade.mapAsList(productDetails,ProductDetailResponseVM.class));
            return ResponseEntity.ok(result);
        }catch (Exception e){
            throw new FormValidateException("error",e.getMessage());
        }

    }

    @PutMapping("/products/update_status/product_details")
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN + "\",\"" + AuthoritiesConstants.PRODUCT + "\")")
    public ResponseEntity<String> updateStatusProductDetail(@RequestBody @Valid UpdateStatusVM productRequestVM) {
       return ResponseEntity.ok(productDetailService.updateStatusProductDetail(productRequestVM));
    }
    @PutMapping("/products/update_status")
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN + "\",\"" + AuthoritiesConstants.PRODUCT + "\")")
    public ResponseEntity<String> updateStatusProduct(@RequestBody @Valid UpdateStatusVM productRequestVM) {
        return ResponseEntity.ok(productService.updateStatus(productRequestVM));
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<ProductResponseVM> update(@PathVariable("id") Long id, @RequestBody @Valid
        ProductUpdateRequest productUpdateRequest) {
        try {
            return ResponseEntity.ok(mapperFacade.map(productService.update(id,productUpdateRequest),ProductResponseVM.class));
        }catch (Exception e){
            throw new FormValidateException("error",e.getMessage());
        }

    }
    @PutMapping("/products/default_image/{id}")
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN + "\",\"" + AuthoritiesConstants.PRODUCT + "\")")
    public ResponseEntity<?> updateDefault(@PathVariable("id") Long id, @RequestBody @Valid
        UpdateDefaultImageVM updateDefaultImageVM) {
        try {
            if(updateDefaultImageVM.getType().equals("product")){
                ProductDomain productDomain = productService.findById(id);
                productDomain.setDefaultImage(updateDefaultImageVM.getDefaultImage());
                productRepository.save(productDomain);
            }else{
                ProductDetailDomain productDetailDomain = productDetailService.finById(id);
                productDetailDomain.setDefaultImage(updateDefaultImageVM.getDefaultImage());
                productDetailRepository.save(productDetailDomain);
            }
            return ResponseEntity.ok().build();
        }catch (Exception e){
            throw new FormValidateException("error",e.getMessage());
        }

    }
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN + "\",\"" + AuthoritiesConstants.PRODUCT + "\")")
    @PostMapping("/products/product_detail/{id}")
    public ResponseEntity<ProductDetailResponseVM> createProductDetail(@PathVariable("id") Long id, @RequestBody @Valid
        ProductDetailCreateRequest productDetailCreateRequest) {
        try {
            ProductDomain productDomain = productService.findById(id);
            ProductDetailDomain productDetailDomain = mapperFacade.map(productDetailCreateRequest,ProductDetailDomain.class);
            productDetailDomain.setProductId(productDomain.getId());
            productDetailDomain.setProductCode(productDomain.getProductCode());
            productDetailDomain.setProductName(productDetailCreateRequest.getProductDetailName());
            return ResponseEntity.ok(mapperFacade.map(productDetailRepository.save(productDetailDomain),ProductDetailResponseVM.class));
        }catch (Exception e){
            throw new FormValidateException("error",e.getMessage());
        }

    }
}
