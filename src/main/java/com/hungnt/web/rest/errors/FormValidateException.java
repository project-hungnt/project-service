package com.hungnt.web.rest.errors;

import com.google.common.collect.ImmutableMap;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

public class FormValidateException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    protected final ImmutableMap<String, Object> fieldErrors;

    public FormValidateException(Map<String, Object> fieldErrors) {
        super(buildMessage(fieldErrors));
        this.fieldErrors = ImmutableMap.copyOf(fieldErrors);
    }

    public FormValidateException(String key, Object message) {
        super(String.format("%s: %s", key, message));
        this.fieldErrors = ImmutableMap.of(key, message);
    }

    public FormValidateException(BindingResult bindingResult) {
        super(buildMessage(bindingResult));
        this.fieldErrors = ImmutableMap.copyOf(buildFieldErrors(bindingResult));
    }

    private static String buildMessage(Map<String, Object> fieldErrors) {
        if (fieldErrors == null || fieldErrors.isEmpty()) {
            return null;
        }
        return fieldErrors.entrySet().stream()
            .map(entry -> String.format("%s: %s", entry.getKey(), entry.getValue()))
            .collect(Collectors.joining(", "));
    }

    private static String buildMessage(BindingResult bindingResult) {
        if (bindingResult == null || !bindingResult.hasFieldErrors()) {
            return null;
        }
        return bindingResult.getFieldErrors().stream()
            .map(fieldError -> String.format("%s: %s", fieldError.getField(), fieldError.getDefaultMessage()))
            .collect(Collectors.joining(", "));
    }

    public Map<String, Object> getFieldErrors() {
        return fieldErrors;
    }

    private Map<String, Object> buildFieldErrors(BindingResult bindingResult) {
        Map<String, Object> fieldErrors = new HashMap<>();
        if (bindingResult == null || !bindingResult.hasFieldErrors()) {
            return fieldErrors;
        }
        return bindingResult.getFieldErrors().stream()
            .collect(Collectors.toMap(FieldError::getField, DefaultMessageSourceResolvable::getDefaultMessage));
    }

}
