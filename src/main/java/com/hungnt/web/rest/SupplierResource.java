package com.hungnt.web.rest;

import com.hungnt.domain.supplier.SupplierDomain;
import com.hungnt.repository.SupplierRepository;
import com.hungnt.security.AuthoritiesConstants;
import com.hungnt.service.supplier.SupplierService;
import com.hungnt.web.rest.errors.FormValidateException;
import com.hungnt.web.rest.vm.common.DataCollectionVM;
import com.hungnt.web.rest.vm.common.UpdateStatusVM;
import com.hungnt.web.rest.vm.filter.SupplierFilter;
import com.hungnt.web.rest.vm.supplier.SupplierResponseVM;
import com.hungnt.web.rest.vm.supplier.SupplierVM;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import ma.glasnost.orika.MapperFacade;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@Slf4j
public class SupplierResource {
    private final SupplierService supplierService;
    private final SupplierRepository supplierRepository;
    private final MapperFacade mapperFacade;

    public SupplierResource(SupplierService supplierService,
        SupplierRepository supplierRepository, MapperFacade mapperFacade) {
        this.supplierService = supplierService;
        this.supplierRepository = supplierRepository;
        this.mapperFacade = mapperFacade;
    }

    @GetMapping("/suppliers")
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN + "\",\"" + AuthoritiesConstants.SUPPLIER + "\")")
    public ResponseEntity<DataCollectionVM<SupplierResponseVM>> getCustomers(SupplierFilter filter,
        Pageable pageable) {
        log.info("Request To Filter And Search customer {}", filter.toString());
        Page<SupplierDomain> supplierDomains = supplierService.findByCriteria(filter, pageable);
        val result = new DataCollectionVM<SupplierResponseVM>();
        result.setData(
            supplierDomains.map(m -> mapperFacade.map(m, SupplierResponseVM.class)).getContent());
        result.setLimit(pageable.getPageSize());
        result.setPage(pageable.getPageNumber());
        result.setTotal(supplierDomains.getTotalElements());
        return ResponseEntity.ok().body(result);
    }
    @PostMapping("/suppliers")
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN + "\",\"" + AuthoritiesConstants.SUPPLIER + "\")")
    public ResponseEntity<SupplierResponseVM> create(@RequestBody @Valid SupplierVM supplierVM){
        try {
            log.info("create supplier {}",supplierVM);
            SupplierDomain supplierDomain = supplierService.save(mapperFacade.map(supplierVM,SupplierDomain.class));
            return ResponseEntity.ok(mapperFacade.map(supplierDomain,SupplierResponseVM.class));
        }catch (Exception e){
            log.info("error: {}",e.getMessage());
            throw  new FormValidateException("error",e.getMessage());
        }
    }
    @PutMapping("/suppliers/{id}")
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN + "\",\"" + AuthoritiesConstants.SUPPLIER + "\")")
    public ResponseEntity<SupplierResponseVM> update(@RequestBody @Valid SupplierVM supplierVM,@PathVariable(name = "id") long id){
        try {
            log.info("create supplier {}",supplierVM);
            SupplierDomain supplierDomain = supplierService.getById(id).update(supplierVM) ;
            return ResponseEntity.ok(mapperFacade.map(supplierRepository.save(supplierDomain),SupplierResponseVM.class));
        }catch (Exception e){
            log.info("error: {}",e.getMessage());
            throw  new FormValidateException("error",e.getMessage());
        }
    }
    @GetMapping("/suppliers/code/{code}")
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN + "\",\"" + AuthoritiesConstants.SUPPLIER + "\")")
    public ResponseEntity<SupplierResponseVM> update(@PathVariable(name = "code") String code){
        try {
            log.info("find  supplier by code {}",code);
            SupplierDomain supplierDomain = supplierService.getByCode(code) ;
            return ResponseEntity.ok(mapperFacade.map(supplierDomain,SupplierResponseVM.class));
        }catch (Exception e){
            log.info("error: {}",e.getMessage());
            throw  new FormValidateException("error",e.getMessage());
        }
    }

    @PutMapping("/suppliers/update_status/{id}")
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN + "\",\"" + AuthoritiesConstants.SUPPLIER + "\")")
    public ResponseEntity<SupplierResponseVM> updateStatus(@PathVariable("id") Long id, @RequestBody @Valid UpdateStatusVM updateStatusVM){
        try {
            log.info("update category {}",updateStatusVM);
            SupplierDomain supplierDomain = supplierService.getById(id);
            supplierDomain.setStatus(updateStatusVM.getStatus());
            supplierDomain=supplierRepository.save(supplierDomain);
            return ResponseEntity.ok(mapperFacade.map(supplierDomain,SupplierResponseVM.class));
        }catch (Exception e){
            throw new FormValidateException("error",e.getMessage());
        }
    }
}
