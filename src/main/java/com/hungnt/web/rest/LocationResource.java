package com.hungnt.web.rest;

import com.hungnt.domain.LocationDomain;
import com.hungnt.repository.LocationRepository;
import com.hungnt.web.rest.errors.FormValidateException;
import com.hungnt.web.rest.vm.location.LocationRequestVM;
import com.hungnt.web.rest.vm.location.LocationResponse;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MapperFacade;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@Slf4j
public class LocationResource {

    private final LocationRepository locationRepository;
    private final MapperFacade mapper;

    public LocationResource(LocationRepository locationRepository,
        MapperFacade mapper) {
        this.locationRepository = locationRepository;
        this.mapper = mapper;
    }

    @GetMapping("/locations")
    public ResponseEntity<List<LocationResponse>> getLocations() {
        log.info("filter  location {}");
       List<LocationDomain> locationDomainList = locationRepository.findAll();
        return ResponseEntity.ok().body(mapper.mapAsList(locationDomainList,LocationResponse.class));
    }

    @PostMapping("/locations")
    public ResponseEntity<LocationResponse> createLocation(
        @RequestBody @Valid LocationRequestVM locationRequestVM) {
        try {
            log.info("create category {}", locationRequestVM);
            LocationDomain locationDomain = mapper.map(locationRequestVM, LocationDomain.class);
            locationDomain=locationDomain.crate(locationDomain);
            return ResponseEntity
                .ok(mapper.map(locationRepository.save(locationDomain), LocationResponse.class));
        } catch (Exception e) {
            throw new FormValidateException("error", e.getMessage());
        }
    }

    @PutMapping("/locations/{id}")
    public ResponseEntity<LocationResponse> updateLocation(@PathVariable("id") Long id,
        @RequestBody @Valid LocationRequestVM locationRequestVM) {
        try {
            log.info("update category {}", locationRequestVM);
            Optional<LocationDomain> categoryDomainOptional = locationRepository.findById(id);
            if (categoryDomainOptional.isEmpty()) {
                throw new FormValidateException("error", "not found");
            }
            LocationDomain locationDomain = categoryDomainOptional.get().update(locationRequestVM);
            return ResponseEntity
                .ok(mapper.map(locationRepository.save(locationDomain), LocationResponse.class));
        } catch (Exception e) {
            throw new FormValidateException("error", e.getMessage());
        }
    }

    @GetMapping("/locations/{id}")
    public ResponseEntity<LocationResponse> getLocation(@PathVariable("id") Long id) {
        try {
            log.info("get category {}", id);
            Optional<LocationDomain> categoryDomainOptional = locationRepository.findById(id);
            if (categoryDomainOptional.isEmpty()) {
                throw new FormValidateException("error", "not found");
            }
            LocationDomain locationDomain = categoryDomainOptional.get();
            return ResponseEntity.ok(mapper.map(locationDomain, LocationResponse.class));
        } catch (Exception e) {
            throw new FormValidateException("error", e.getMessage());
        }
    }
}
