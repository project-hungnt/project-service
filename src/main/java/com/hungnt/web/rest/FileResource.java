package com.hungnt.web.rest;

import com.hungnt.domain.FileUpload;
import com.hungnt.firebase.FirebaseInitializeApp;
import com.hungnt.repository.FileUploadRepository;
import com.hungnt.service.common.FileUpLoadService;
import com.hungnt.web.rest.errors.FormValidateException;
import com.hungnt.web.rest.vm.fileUpload.FileRequestWrap;
import com.hungnt.web.rest.vm.fileUpload.FileUploadResponse;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import javassist.NotFoundException;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MapperFacade;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@Slf4j
public class FileResource {

    private final MapperFacade mapper;
    private final FirebaseInitializeApp firebaseInitialize;
    private final FileUploadRepository uploadRepository;
    private final FileUpLoadService fileUpLoadService;

    public FileResource(MapperFacade mapper, FirebaseInitializeApp firebaseInitialize,
        FileUploadRepository uploadRepository,
        FileUpLoadService fileUpLoadService) {
        this.mapper = mapper;
        this.firebaseInitialize = firebaseInitialize;
        this.uploadRepository = uploadRepository;
        this.fileUpLoadService = fileUpLoadService;
    }

    @PostMapping("/upload_files")
    public ResponseEntity<List<FileUploadResponse>> uploadFile(
        @RequestBody @Valid FileRequestWrap fileRequest) throws IOException {
        log.info("upload files {}",fileRequest);
        List<FileUpload> fileUploads = uploadRepository
            .saveAll(firebaseInitialize.uploadFile(fileRequest));
        return ResponseEntity.ok(fileUploads.stream()
            .filter(Objects::nonNull)
            .map(fileUpload -> mapper.map(fileUpload, FileUploadResponse.class)).collect(
                Collectors.toList()));
    }

    @GetMapping("/upload_files/{provider}/reference/{referenceId}")
    public ResponseEntity<List<FileUploadResponse>> getFiles(@PathVariable("provider") String provider,@PathVariable("referenceId") Long referenceId) {
        return getListFileByReference(provider, referenceId);
    }

    @GetMapping("/upload_files/{id}")
    public ResponseEntity<FileUploadResponse> getFile(
        @PathVariable("id") Long id) throws NotFoundException {
        Optional<FileUpload> fileUpload = uploadRepository.findById(id);
        if(fileUpload.isEmpty() || fileUpload.get().isDeleted()){
            throw new NotFoundException("Không tìm thấy hoặc file đã bị xóa trước đó");
        }
        return ResponseEntity.ok(mapper.map(fileUpload, FileUploadResponse.class));
    }
    @DeleteMapping("/upload_files/remove/{id}")
    public ResponseEntity<?> deleteFile(@PathVariable("id") Long id) {
        log.debug("Request delete file {}", id);
        uploadRepository.findById(id).ifPresent(file -> {
            file.setDeleted(true);
            uploadRepository.save(file);
            log.info("Deleted File: {}", file);
        });
        return ResponseEntity.ok().build();
    }
    @DeleteMapping("/upload_files/{provider}/reference/{referenceId}")
    public ResponseEntity<?> deleteAllFileByProvider(@PathVariable("provider") String provider,@PathVariable("referenceId") Long referenceId) {
        List<FileUpload> fileUploads = fileUpLoadService.getAllFileByProvider(provider,referenceId);
        fileUploads.forEach(fileUpload -> {
            fileUpload.setDeleted(true);
        });
        uploadRepository.saveAll(fileUploads);
        return ResponseEntity.ok().build();
    }
    @DeleteMapping("/upload_files/{provider}/reference/{referenceId}/remove/{id}")
    public ResponseEntity<List<FileUploadResponse>> deleteAndReturnFiles(@PathVariable("provider") String provider,@PathVariable("referenceId") Long referenceId,@PathVariable("id") Long id) {
        log.debug("Request delete file {}", id);
        uploadRepository.findById(id).ifPresent(file -> {
            file.setDeleted(true);
            uploadRepository.save(file);
            log.info("Deleted File: {}", file);
        });
        return getListFileByReference(provider, referenceId);
    }

    private ResponseEntity<List<FileUploadResponse>> getListFileByReference(String provider,Long referenceId) {
        if(StringUtils.isBlank(provider)){
            throw new FormValidateException("error","Không xác định được loại file");
        }
        List<FileUpload> fileUploads = fileUpLoadService.getAllFileByProvider(provider,referenceId);
        return ResponseEntity
            .ok(fileUploads
                .stream()
                .filter(fileUpload -> !fileUpload.isDeleted())
                .map(fileUpload -> mapper.map(fileUpload, FileUploadResponse.class))
                .collect(Collectors.toList()));
    }
}
