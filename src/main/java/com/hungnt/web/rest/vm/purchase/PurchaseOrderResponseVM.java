package com.hungnt.web.rest.vm.purchase;

import com.hungnt.web.rest.vm.common.BaseResponse;
import com.hungnt.web.rest.vm.customer.CustomerResponseVM;
import com.hungnt.web.rest.vm.supplier.SupplierVM;
import com.hungnt.web.rest.vm.user.UserResponseNotAuthorizes;
import java.math.BigDecimal;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class PurchaseOrderResponseVM extends BaseResponse {
    private static final long serialVersionUID = 1L;
    private BigDecimal totalPrice;
    private Integer totalQuantity;
    private Long userId;
    private SupplierVM supplier;
    private Long supplierId;
    private BigDecimal money;
    private String note;
    private String paymentMethod;
    private String status;
    private String code;
    private UserResponseNotAuthorizes user;
}
