package com.hungnt.web.rest.vm.qr;

import java.math.BigDecimal;
import javax.validation.constraints.NotBlank;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class QrVmResponse {
    private String bankId;
    private String accountNumber;
    private String template;
    private BigDecimal amount;
    private String description;
    private String accountName;
    private Long referenceId;
    private String link;
    @Override
    public String toString() {
        return "VietQrVM{" +
            "bankId='" + bankId + '\'' +
            ", accountNumber='" + accountNumber + '\'' +
            ", template='" + template + '\'' +
            ", amount=" + amount +
            ", description='" + description + '\'' +
            ", accountName='" + accountName + '\'' +
            '}';
    }
}
