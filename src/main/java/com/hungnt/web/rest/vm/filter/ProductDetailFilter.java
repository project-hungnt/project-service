package com.hungnt.web.rest.vm.filter;

import com.hungnt.web.rest.vm.common.BaseFilter;
import java.math.BigDecimal;
import java.util.List;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class ProductDetailFilter extends BaseFilter {
    private List<Long> locationIds;
    private List<Long> categories;
    private BigDecimal priceMin;
    private BigDecimal priceMax;
    private BigDecimal priceSellMin;
    private BigDecimal priceSellMax;
    private Integer quantityMin;
    private Integer quantityMax;
    private Integer quantitySellMin;
    private Integer quantitySellMax;
    private List<String> status;

    @Override
    public String toString() {
        return "ProductDetailFilter{" +
            "locationIds=" + locationIds +
            ", categoryIds=" + categories +
            ", priceMin=" + priceMin +
            ", priceMax=" + priceMax +
            ", priceSellMin=" + priceSellMin +
            ", priceSellMax=" + priceSellMax +
            ", quantityMin=" + quantityMin +
            ", quantityMax=" + quantityMax +
            ", quantitySellMin=" + quantitySellMin +
            ", quantitySellMax=" + quantitySellMax +
            ", status=" + status +
            '}';
    }
}
