package com.hungnt.web.rest.vm.customer;

import com.hungnt.web.rest.vm.common.BaseResponse;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class CustomerResponseVM extends BaseResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    private String code;
    private String phone;
    private String gender;
    private String name;
    private String birth;
    private String email;
    private String cityCode;
    private String wardCode;
    private String districtCode;
    private String cityName;
    private String wardName;
    private String districtName;
    private String address;
    private Integer totalOrders;
    private BigDecimal totalSales;
}
