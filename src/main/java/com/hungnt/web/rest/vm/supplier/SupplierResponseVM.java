package com.hungnt.web.rest.vm.supplier;

import com.hungnt.config.Constants;
import com.hungnt.web.rest.vm.common.BaseResponse;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class SupplierResponseVM extends BaseResponse {
    private Long id;
    private String code;
    private String name;
    private String description;
    private String contactPhone;
    private String contactName;
    private String contactEmail;
    private String cityCode;
    private String wardCode;
    private String districtCode;
    private String cityName;
    private String wardName;
    private String districtName;
    private String address;
    private String bankCode;
    private String bankName;
    private String accountNumber;
    private String accountName;
    private String bankBranch;
    private String status= Constants.STATUS_ACTIVE;
}
