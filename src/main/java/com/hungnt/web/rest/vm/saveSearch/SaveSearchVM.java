package com.hungnt.web.rest.vm.saveSearch;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonRootName;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@JsonRootName("save_search")
@JsonIgnoreProperties(ignoreUnknown = true)
public class SaveSearchVM implements Serializable {
    private Long id;
    @NotBlank
    @Size(max = 50)
    private String type;
    @NotBlank
    @Size(max = 255)
    private String name;
    @NotBlank
    @Size(max = 10000)
    private String jsonContent;
    @Override
    public String toString() {
        return "VMSaveSearchCreate{" +
            "type=" + type +
            ", name='" + name + '\'' +
            ", jsonFilter='" + jsonContent + '\'' +
            '}';
    }
}
