package com.hungnt.web.rest.vm.product;

import com.hungnt.config.Constants;
import com.hungnt.web.rest.vm.common.BaseResponse;
import java.math.BigDecimal;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class ProductDetailResponseVM extends BaseResponse {
    private String productName;
    private String productCode;
    private Long categoryId;
    private Long productId;
    private Long locationId;
    private String size;
    private String color;
    private String code;
    private String material;
    private BigDecimal price= BigDecimal.valueOf(0);
    private String status = Constants.STATUS_ACTIVE;
    private String barCode;
    private BigDecimal priceSell=BigDecimal.valueOf(0);
    private Integer discount;
    private int quantity=0;
    private Integer quantitySell=0;
    private String defaultImage;
}
