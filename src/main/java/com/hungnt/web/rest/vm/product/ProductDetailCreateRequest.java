package com.hungnt.web.rest.vm.product;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.hungnt.config.Constants;
import com.hungnt.web.rest.vm.fileUpload.FileCreateVM;
import java.math.BigDecimal;
import javax.validation.constraints.Min;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@JsonRootName("product_details")
public class ProductDetailCreateRequest {
    private Long productId;
    private String size;
    private String color;
    @Min(0)
    private BigDecimal price= BigDecimal.valueOf(0);
    private String status = Constants.STATUS_ACTIVE;
    private BigDecimal priceSell=BigDecimal.valueOf(0);
    private Integer discount;
    private int quantity=0;
    private Integer quantitySell=0;
    private String productDetailName;
}
