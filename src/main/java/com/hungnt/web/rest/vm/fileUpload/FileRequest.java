package com.hungnt.web.rest.vm.fileUpload;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class FileRequest {
    @NotBlank
    private String base64;
    @NotBlank
    @Size(max = 255)
    private String fileName;
    @Override
    public String toString() {
        return "FileRequest{" +
            "base64='" + base64 + '\'' +
            ", fileName='" + fileName + '\'' +
            '}';
    }
}
