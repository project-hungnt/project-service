package com.hungnt.web.rest.vm.location;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.hungnt.config.Constants;
import javax.validation.constraints.NotBlank;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@JsonRootName("location")
public class LocationRequestVM {
    @NotBlank
    private String name;
    @NotBlank
    private String cityName;
    @NotBlank
    private String wardName;
    @NotBlank
    private String districtName;
    @NotBlank
    private String address;
    private String status = Constants.STATUS_ACTIVE;
}
