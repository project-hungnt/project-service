package com.hungnt.web.rest.vm.qr;

import com.fasterxml.jackson.annotation.JsonProperty;
public class BankVM {
    private Long id;
    private String name;
    private String code;
    private String bin;
    @JsonProperty("is_transfer")
    private int isTransfer;
    @JsonProperty("short_name")
    private String shortName;
    private String logo;
    private int support;

    public BankVM() {
    }

    public BankVM(Long id, String name, String code, String bin, int isTransfer,
        String shortName, String logo, int support) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.bin = bin;
        this.isTransfer = isTransfer;
        this.shortName = shortName;
        this.logo = logo;
        this.support = support;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getBin() {
        return bin;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }

    public int getIsTransfer() {
        return isTransfer;
    }

    public void setIsTransfer(int isTransfer) {
        this.isTransfer = isTransfer;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public int getSupport() {
        return support;
    }

    public void setSupport(int support) {
        this.support = support;
    }

    @Override
    public String toString() {
        return "VietQrBank{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", code='" + code + '\'' +
            ", bin='" + bin + '\'' +
            ", isTransfer=" + isTransfer +
            ", short_name='" + shortName + '\'' +
            ", logo='" + logo + '\'' +
            ", support=" + support +
            '}';
    }
}
