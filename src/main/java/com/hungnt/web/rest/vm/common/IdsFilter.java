package com.hungnt.web.rest.vm.common;

import com.fasterxml.jackson.annotation.JsonRootName;
import java.util.List;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class IdsFilter {
    private List<Long> ids;
}
