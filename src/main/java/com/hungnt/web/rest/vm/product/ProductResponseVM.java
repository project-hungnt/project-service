package com.hungnt.web.rest.vm.product;

import com.hungnt.web.rest.vm.common.BaseResponse;
import java.util.List;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class ProductResponseVM extends BaseResponse {
    private String productName;
    private String productCode;
    private Long categoryId;
    private Long locationId;
    private String description;
    private String defaultImage;
    private String status;
    private List<ProductDetailResponseVM> productDetails;
}
