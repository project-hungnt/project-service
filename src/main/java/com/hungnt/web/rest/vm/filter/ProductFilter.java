package com.hungnt.web.rest.vm.filter;

import com.hungnt.web.rest.vm.common.BaseFilter;
import java.util.List;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class ProductFilter extends BaseFilter {
    private List<Long> locations;
    private List<Long> categories;
    private List<String> status;
}
