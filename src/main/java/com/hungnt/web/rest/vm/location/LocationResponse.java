package com.hungnt.web.rest.vm.location;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.hungnt.web.rest.vm.common.BaseResponse;
import lombok.Data;

@Data
@JsonRootName("location")
public class LocationResponse extends BaseResponse {
    private Long id;

    private String name;
    private String cityCode;
    private String wardCode;
    private String districtCode;
    private String cityName;
    private String wardName;
    private String districtName;
    private String address;
    private String status;
}
