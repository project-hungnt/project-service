package com.hungnt.web.rest.vm.fileUpload;

import com.hungnt.web.rest.vm.common.BaseResponse;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class FileUploadResponse extends BaseResponse {
    private Long id;
    private String fileType;
    private String fileUrl;
    private String fileName;
    private String fileExtension;
    private boolean deleted;
    private Long referenceId;
    private String provider;
}
