package com.hungnt.web.rest.vm.product;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.hungnt.config.Constants;
import com.hungnt.web.rest.vm.fileUpload.FileRequest;
import java.math.BigDecimal;
import java.util.List;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@JsonRootName("product")
public class ProductRequestVM {
    private Long id;
    @NotBlank
    @Size(max = 255)
    private String productName;
    private Long categoryId;
    private Long locationId;
    private int quantity=0;
    private String description;
    private String status= Constants.STATUS_ACTIVE;
    private BigDecimal priceSell=BigDecimal.valueOf(0);
    private BigDecimal price= BigDecimal.valueOf(0);
    private List<ProductRequestDetailVM>  productDetails;
    private List<FileRequest> files;
}
