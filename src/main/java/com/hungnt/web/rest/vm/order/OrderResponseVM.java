package com.hungnt.web.rest.vm.order;

import com.hungnt.web.rest.vm.common.BaseResponse;
import com.hungnt.web.rest.vm.customer.CustomerResponseVM;
import com.hungnt.web.rest.vm.user.UserResponseNotAuthorizes;
import com.hungnt.web.rest.vm.user.UserResponseVM;
import java.math.BigDecimal;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class OrderResponseVM extends BaseResponse {
    private static final long serialVersionUID = 1L;
    private BigDecimal totalPrice;
    private Integer totalQuantity;
    private Long userId;
    private CustomerResponseVM customer;
    private Long customerId;
    private BigDecimal money;
    private String note;
    private String paymentMethod;
    private String status;
    private String code;
    private UserResponseNotAuthorizes user;
    private BigDecimal discount;
}
