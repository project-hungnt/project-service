package com.hungnt.web.rest.vm.filter;

import com.hungnt.web.rest.vm.common.BaseFilter;
import java.util.List;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class UserFilter extends BaseFilter {

    private List<String> statuses;
    private List<String> roleTypes;
}
