package com.hungnt.web.rest.vm.common;

import java.time.Instant;

public class BaseFilter {
    protected String query;
    protected Instant createdDateMin;
    protected Instant createdDateMax;
    protected Instant lastModifiedDateMin;
    protected Instant lastModifiedDateMax;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Instant getCreatedDateMin() {
        return createdDateMin;
    }

    public void setCreatedDateMin(Instant createdDateMin) {
        this.createdDateMin = createdDateMin;
    }

    public Instant getCreatedDateMax() {
        return createdDateMax;
    }

    public void setCreatedDateMax(Instant createdDateMax) {
        this.createdDateMax = createdDateMax;
    }

    public Instant getLastModifiedDateMin() {
        return lastModifiedDateMin;
    }

    public void setLastModifiedDateMin(Instant lastModifiedDateMin) {
        this.lastModifiedDateMin = lastModifiedDateMin;
    }

    public Instant getLastModifiedDateMax() {
        return lastModifiedDateMax;
    }

    public void setLastModifiedDateMax(Instant lastModifiedDateMax) {
        this.lastModifiedDateMax = lastModifiedDateMax;
    }
}
