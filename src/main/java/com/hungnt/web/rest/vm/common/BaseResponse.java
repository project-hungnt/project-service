package com.hungnt.web.rest.vm.common;

import java.io.Serializable;
import java.time.Instant;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class BaseResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private Instant createdDate;
    private String createdBy;
    private Instant lastModifiedDate;
    private String lastModifiedBy;
}
