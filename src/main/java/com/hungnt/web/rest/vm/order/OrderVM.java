package com.hungnt.web.rest.vm.order;

import com.fasterxml.jackson.annotation.JsonRootName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@JsonRootName("order")
public class OrderVM implements Serializable {
    private static final long serialVersionUID = 1L;
    private BigDecimal totalPrice;
    private Integer totalQuantity;
    private Long userId;
    @Valid
    private List<OrderDetailVM> orderDetails;
    private Long customerId;
    @NotNull(message = "Money is required")
    private BigDecimal money;
    private String note;
    private String paymentMethod;
    private String status;
    private String code;
    private BigDecimal discount;
}
