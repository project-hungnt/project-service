package com.hungnt.web.rest.vm.category;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.hungnt.config.Constants;
import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@JsonRootName("category")
public class CategoryRequestVM implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @NotBlank
    private String categoryName;
    private String categoryCode;
    private String description;
    private String status= Constants.STATUS_ACTIVE;
}
