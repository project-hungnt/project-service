package com.hungnt.web.rest.vm.common;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@JsonRootName("update_status")
public class UpdateStatusVM {
    private String status;
    private Long id;
}
