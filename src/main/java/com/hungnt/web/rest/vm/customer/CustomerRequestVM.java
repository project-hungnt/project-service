package com.hungnt.web.rest.vm.customer;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.hungnt.config.Constants;
import java.io.Serializable;
import java.time.Instant;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@JsonRootName("customer")
public class CustomerRequestVM implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private String code;
    private String phone;

    private String gender="Male";

    private String name;

    @Size(max = 20)
    private String birth;

    private String email;

    private String cityName;

    private String wardName;

    private String districtName;

    private String address;
}
