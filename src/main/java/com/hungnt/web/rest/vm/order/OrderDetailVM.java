package com.hungnt.web.rest.vm.order;

import com.fasterxml.jackson.annotation.JsonRootName;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@JsonRootName("order_detail")
public class OrderDetailVM {
    @NotNull
    private int quantity=0;
    @NotNull
    private BigDecimal price=BigDecimal.valueOf(0);
    @NotNull
    private Long productDetailId;
    @NotNull
    private BigDecimal discount=BigDecimal.valueOf(0);
}
