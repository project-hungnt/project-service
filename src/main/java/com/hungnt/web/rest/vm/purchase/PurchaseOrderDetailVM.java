package com.hungnt.web.rest.vm.purchase;

import com.fasterxml.jackson.annotation.JsonRootName;
import java.math.BigDecimal;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@JsonRootName("purchase_order_detail")
public class PurchaseOrderDetailVM {
    @NotNull
    private int quantity=0;
    @NotNull
    private BigDecimal price=BigDecimal.valueOf(0);
    @NotNull
    private Long productDetailId;
}
