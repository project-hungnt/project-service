package com.hungnt.web.rest.vm.product;

import com.fasterxml.jackson.annotation.JsonRootName;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@JsonRootName("default_image")
public class UpdateDefaultImageVM {
    @NotNull
    private Long id;
    @NotBlank
    private String defaultImage;
    @NotBlank
    private String type;
}
