package com.hungnt.web.rest.vm.saveSearch;

import com.hungnt.web.rest.vm.common.BaseResponse;
import java.io.Serializable;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class SaveSearchResponseVM extends BaseResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    private String type;
    private String name;
    private String jsonContent;

    @Override
    public String toString() {
        return "SaveSearchResponse{" +
            "id=" + getId() +
            ", type=" + getType() +
            ", name='" + getName() + '\'' +
            ", jsonContent='" + getJsonContent() + '\'' +
            ", createdDate=" + getCreatedDate() +
            ", createdBy='" + getCreatedBy() + '\'' +
            ", lastModifiedDate=" + getLastModifiedDate() +
            ", lastModifiedBy='" + getLastModifiedBy() + '\'' +
            '}';
    }
}
