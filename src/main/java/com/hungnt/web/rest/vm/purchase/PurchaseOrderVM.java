package com.hungnt.web.rest.vm.purchase;

import com.fasterxml.jackson.annotation.JsonRootName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@JsonRootName("purchase_order")
public class PurchaseOrderVM implements Serializable {
    private static final long serialVersionUID = 1L;
    private BigDecimal totalPrice;
    private Integer totalQuantity;
    private Long userId;
    @Valid
    private List<PurchaseOrderDetailVM> purchaseOrderDetails;
    private Long supplierId;
    @NotNull(message = "Money is required")
    private BigDecimal money;
    private String note;
    private String paymentMethod;
    private String status;
    private String code;

}
