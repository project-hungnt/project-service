package com.hungnt.web.rest.vm.common;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class DistrictResponseVM {
    private Long id;
    private String districtName;
    private String districtCode;
    private String cityCode;
}
