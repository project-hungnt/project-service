package com.hungnt.web.rest.vm.category;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.hungnt.web.rest.vm.common.BaseResponse;
import java.io.Serializable;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@JsonRootName("category")
public class CategoryResponseVM extends BaseResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private String categoryCode;
    private String categoryName;
    private String description;
    private String status;
}
