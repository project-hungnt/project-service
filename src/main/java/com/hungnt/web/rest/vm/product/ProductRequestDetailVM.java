package com.hungnt.web.rest.vm.product;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.hungnt.config.Constants;
import com.hungnt.web.rest.vm.fileUpload.FileCreateVM;
import java.math.BigDecimal;
import javax.validation.constraints.Min;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@JsonRootName("product_detail")
public class ProductRequestDetailVM {
    private Long id;
    private Long categoryId;
    private Long productId;
    private String size;
    private String color;
    private String material;
    @Min(0)
    private BigDecimal price= BigDecimal.valueOf(0);
    private String status = Constants.STATUS_ACTIVE;
    private String barCode;
    private BigDecimal priceSell=BigDecimal.valueOf(0);
    private Integer discount;
    private int quantity=0;
    private Integer quantitySell=0;
    private Long locationId;
    private String productName;
    private String productDetailName;
    private FileCreateVM fileCreateVM;
}
