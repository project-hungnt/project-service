package com.hungnt.web.rest.vm.purchase;

import com.hungnt.web.rest.vm.common.BaseResponse;
import com.hungnt.web.rest.vm.product.ProductDetailResponseVM;
import java.math.BigDecimal;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class PurchaseOrderDetailResponseVM extends BaseResponse {
    private int quantity;
    private BigDecimal price;
    private Long productDetailId;
    private ProductDetailResponseVM productDetail;
}
