package com.hungnt.web.rest.vm.order;

import com.hungnt.web.rest.vm.common.BaseResponse;
import com.hungnt.web.rest.vm.product.ProductDetailResponseVM;
import java.math.BigDecimal;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class OrderDetailResponseVM extends BaseResponse {
    private int quantity;
    private BigDecimal price;
    private Long productDetailId;
    private BigDecimal discount;
    private ProductDetailResponseVM productDetail;
}
