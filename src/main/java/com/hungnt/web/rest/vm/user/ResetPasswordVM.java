package com.hungnt.web.rest.vm.user;

public class ResetPasswordVM {
    private String email;

    public ResetPasswordVM(String email) {
        this.email = email;
    }

    public ResetPasswordVM() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
