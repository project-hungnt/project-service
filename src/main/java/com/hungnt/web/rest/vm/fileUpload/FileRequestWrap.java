package com.hungnt.web.rest.vm.fileUpload;

import com.fasterxml.jackson.annotation.JsonRootName;
import java.io.Serializable;
import java.util.List;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@JsonRootName("file_request")
@Data
@RequiredArgsConstructor
public class FileRequestWrap implements Serializable {
    private Long referenceId;
    @NotEmpty
    @Size(max = 100)
    private List<FileRequest> fileRequests;

    @NotBlank
    @Size(max = 100)
    private String provider;
}
