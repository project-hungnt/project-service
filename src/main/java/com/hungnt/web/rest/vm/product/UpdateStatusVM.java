package com.hungnt.web.rest.vm.product;

import com.fasterxml.jackson.annotation.JsonRootName;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@JsonRootName("update_status")
public class UpdateStatusVM {
    @NotBlank
    private String status;
    @NotNull
    private Long[] ids;

}
