package com.hungnt.web.rest.vm.common;

import java.io.Serializable;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class CityResponseVM implements Serializable {
    private Long id;
    private String cityName;
    private String cityCode;
}
