package com.hungnt.web.rest.vm.user;

import com.hungnt.web.rest.vm.common.BaseResponse;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class UserResponseNotAuthorizes extends BaseResponse {
    private String login;
    private String email;
    private String fullName;
    private String password;
    private String phoneNumber;
    private String status;

}
