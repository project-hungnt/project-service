package com.hungnt.web.rest.vm.user;

import com.hungnt.web.rest.vm.common.BaseResponse;
import java.util.HashSet;
import java.util.Set;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class UserResponseVM extends BaseResponse {
    private String code;
    private String login;
    private String email;
    private String fullName;
    private String password;
    private String phoneNumber;
    private String status;
    private Set<String> authorities = new HashSet<>();
    private String role;
}
