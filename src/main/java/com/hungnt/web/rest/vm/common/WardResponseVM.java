package com.hungnt.web.rest.vm.common;

import java.io.Serializable;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class WardResponseVM implements Serializable {
    private Long id;
    private String wardName;
    private String wardCode;
    private String cityCode;
    private String districtCode;
}
