package com.hungnt.web.rest.vm.supplier;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.hungnt.config.Constants;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@JsonRootName("supplier")
public class SupplierVM {
    private Long id;
    private String code;

    @Size(max = 255)
    private String name;

    private String description;

    @NotBlank
    @Size(max = 255)
    private String contactPhone;

    @NotBlank
    @Size(max = 255)
    private String contactName;

    @NotBlank
    @Size(max = 255)
    private String contactEmail;

    @Size(max = 255)
    private String cityCode;

    @Size(max = 255)
    private String wardCode;

    private String districtCode;

    @Size(max = 255)
    private String cityName;

    @Size(max = 255)
    private String wardName;

    @Size(max = 255)
    private String districtName;

    @Size(max = 255)
    private String address;

    @NotBlank
    @Size(max = 255)
    private String bankCode;

    private String bankName;

    @NotBlank
    @Size(max = 255)
    private String accountNumber;

    @NotBlank
    @Size(max = 255)
    private String accountName;

    @NotBlank
    @Size(max = 255)
    private String bankBranch;

    private String status= Constants.STATUS_ACTIVE;
}
