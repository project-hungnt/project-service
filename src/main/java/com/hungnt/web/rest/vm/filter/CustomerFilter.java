package com.hungnt.web.rest.vm.filter;

import com.hungnt.web.rest.vm.common.BaseFilter;
import java.math.BigDecimal;
import java.util.List;
import lombok.Data;

@Data
public class CustomerFilter  extends BaseFilter {
    private List<String> cities;
    private Integer orderNumberMax;
    private Integer orderNumberMin;
    private BigDecimal saleMin;
    private BigDecimal saleMax;
}
