package com.hungnt.web.rest.vm.product;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.hungnt.config.Constants;
import java.math.BigDecimal;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@JsonRootName("product_details")
public class ProductDetailUpdateRequest {
    private Long id;
    private Long categoryId;
    private String size;
    private String color;
    private String productName;
    private Integer discount;
    private int quantity=0;
    private Integer quantitySell=0;
    private BigDecimal price= BigDecimal.valueOf(0);
    private String status = Constants.STATUS_ACTIVE;
    private BigDecimal priceSell=BigDecimal.valueOf(0);
}
