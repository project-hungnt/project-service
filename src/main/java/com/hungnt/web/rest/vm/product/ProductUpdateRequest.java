package com.hungnt.web.rest.vm.product;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.hungnt.web.rest.vm.fileUpload.FileRequest;
import java.util.List;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@JsonRootName("product")
public class ProductUpdateRequest {
    private Long id;
    private String productName;
    private Long categoryId;
    private String description;
    private List<ProductDetailUpdateRequest> productDetails;
    private List<FileRequest> files;
}
