package com.hungnt.web.rest.vm.qr;

import com.fasterxml.jackson.annotation.JsonRootName;
import java.math.BigDecimal;
import javax.validation.constraints.NotBlank;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@JsonRootName("qr")
public class CreateQrVM {
    @NotBlank
    private String bankId;
    @NotBlank
    private String accountNumber;
    @NotBlank
    private String template;
    private BigDecimal amount;
    @NotBlank
    private String description;
    @NotBlank
    private String accountName;
    private Long referenceId;
    @Override
    public String toString() {
        return "VietQrVM{" +
            "bankId='" + bankId + '\'' +
            ", accountNumber='" + accountNumber + '\'' +
            ", template='" + template + '\'' +
            ", amount=" + amount +
            ", description='" + description + '\'' +
            ", accountName='" + accountName + '\'' +
            '}';
    }
}
