package com.hungnt.web.rest;

import com.hungnt.common.SaveSearchType;
import com.hungnt.config.Constants;
import com.hungnt.domain.CityDomain;
import com.hungnt.domain.SaveSearchDomain;
import com.hungnt.repository.DistrictRepository;
import com.hungnt.repository.WardRepository;
import com.hungnt.repository.saveSearch.SaveSearchRepository;
import com.hungnt.security.SecurityUtils;
import com.hungnt.service.CityService;
import com.hungnt.service.dto.filter.BasicCriteria;
import com.hungnt.utils.RequestUtils;
import com.hungnt.web.rest.errors.FormValidateException;
import com.hungnt.web.rest.vm.common.CityResponseVM;
import com.hungnt.web.rest.vm.common.DataCollectionVM;
import com.hungnt.web.rest.vm.common.DistrictResponseVM;
import com.hungnt.web.rest.vm.common.WardResponseVM;
import com.hungnt.web.rest.vm.saveSearch.SaveSearchResponseVM;
import com.hungnt.web.rest.vm.saveSearch.SaveSearchVM;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import javassist.NotFoundException;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import ma.glasnost.orika.MapperFacade;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@Slf4j
public class CommonResource {
    private final MapperFacade mapper;
    private final SaveSearchRepository searchRepository;
    private final CityService cityService;
    private final DistrictRepository districtRepository;
    private final WardRepository wardRepository;
    public CommonResource(MapperFacade mapper,
        SaveSearchRepository searchRepository, CityService cityService,
        DistrictRepository districtRepository, WardRepository wardRepository) {
        this.mapper = mapper;
        this.searchRepository = searchRepository;
        this.cityService = cityService;
        this.districtRepository = districtRepository;
        this.wardRepository = wardRepository;
    }
    /**
     * *****************************************************************************************************
     * ************************************ API Contry ***************************************
     * *****************************************************************************************************
     */
    @GetMapping("/cities/query")
    public ResponseEntity<DataCollectionVM<CityResponseVM>> cities(String query, Pageable pageable){
        log.info("Request To Filter And Search City {}", query);
        Pageable pageRequest = RequestUtils.limitPaging(pageable, Sort.by(Direction.ASC, "id"));
        BasicCriteria criteria = cityService.buildCriteria(query);
        Page<CityDomain> cityDomainPage = cityService.findByCriteria(criteria, pageRequest);
        val result = new DataCollectionVM<CityResponseVM>();
        result.setData(cityDomainPage.map(m -> mapper.map(m, CityResponseVM.class)).getContent());
        result.setLimit(pageable.getPageSize());
        result.setPage(pageable.getPageNumber());
        result.setTotal(cityDomainPage.getTotalElements());
        return ResponseEntity.ok().body(result);
    }
    @GetMapping("/cities")
    public ResponseEntity<List<CityResponseVM>> cities(){
      List<CityDomain> cityDomains = cityService.findAll();
      return ResponseEntity.ok(mapper.mapAsList(cityDomains,CityResponseVM.class));
    }
    @GetMapping("/cities/{city_code}/districts")
    public ResponseEntity<List<DistrictResponseVM>> getDistrictsByCityCode(@PathVariable(name = "city_code")  String cityCode){
        List<DistrictResponseVM> results = Optional.ofNullable(districtRepository.findByCityCode(cityCode)).orElseGet(ArrayList::new)
            .stream().filter(Objects::nonNull)
            .map(districtDomain -> mapper.map(districtDomain,DistrictResponseVM.class))
            .collect(Collectors.toList());
        return ResponseEntity.ok(results);
    }
    @GetMapping("/districts")
    public ResponseEntity<List<DistrictResponseVM>> districts(){
        List<DistrictResponseVM> results = Optional.ofNullable(districtRepository.findAll()).orElseGet(ArrayList::new)
            .stream().filter(Objects::nonNull)
            .map(districtDomain -> mapper.map(districtDomain,DistrictResponseVM.class))
            .collect(Collectors.toList());
        return ResponseEntity.ok(results);
    }
    @GetMapping("/cities/{city_code}/districts/{district_code}/wards")
    public ResponseEntity<List<WardResponseVM>> getWardByCityCodeAndDistrictCode(@PathVariable(name = "city_code")  String cityCode,@PathVariable(name = "district_code")  String districtCode){
        List<WardResponseVM> results = Optional.ofNullable(wardRepository.findByCityCodeAndDistrictCode(cityCode,districtCode)).orElseGet(ArrayList::new)
            .stream().filter(Objects::nonNull)
            .map(wardDomain -> mapper.map(wardDomain,WardResponseVM.class))
            .collect(Collectors.toList());
        return ResponseEntity.ok(results);
    }
    @GetMapping("/wards")
    public ResponseEntity<List<WardResponseVM>> wards(){
        List<WardResponseVM> results = Optional.ofNullable(wardRepository.findAll()).orElseGet(ArrayList::new)
            .stream().filter(Objects::nonNull)
            .map(wardDomain -> mapper.map(wardDomain,WardResponseVM.class))
            .collect(Collectors.toList());
        return ResponseEntity.ok(results);
    }
    /**
     * *****************************************************************************************************
     * ************************************ API SAVE SEARCH ***************************************
     * *****************************************************************************************************
     */
    @PostMapping("/save_searches")
    public ResponseEntity<SaveSearchResponseVM> createSaveSearch(@RequestBody @Valid SaveSearchVM searchVM) throws IOException  {
        log.info("Rest request create save search {}", searchVM);
        if(!SaveSearchType.getListType().contains(searchVM.getType())){
            throw new FormValidateException("error","Type không nằm trong danh sách cho phép");
        }
        SaveSearchDomain savedSearch = mapper.map(searchVM, SaveSearchDomain.class);
        savedSearch =searchRepository.saveAndFlush(savedSearch);
        SaveSearchResponseVM saveSearchResponse = mapper.map(savedSearch, SaveSearchResponseVM.class);
        return ResponseEntity.ok().body(saveSearchResponse);

    }
    @PutMapping("/save_searches/{id}")
    public ResponseEntity<SaveSearchResponseVM> updateSaveSearch(@PathVariable(name = "id") Long id, @RequestBody @Valid SaveSearchVM searchVM)
        throws NotFoundException {
        log.info("Rest request update save search {}", searchVM);
        if(!SaveSearchType.getListType().contains(searchVM.getType())){
            throw new FormValidateException("error","Type không nằm trong danh sách cho phép");
        }
        Optional<SaveSearchDomain> optionalSaveSearchDomain = searchRepository.findById(id);
        if(optionalSaveSearchDomain.isEmpty()){
            throw new NotFoundException("Không tìm thấy đối tượng");
        }
        SaveSearchDomain searchDomain = optionalSaveSearchDomain.get().update(searchVM);
        searchDomain =searchRepository.save(searchDomain);
        SaveSearchResponseVM saveSearchResponse = mapper.map(searchDomain, SaveSearchResponseVM.class);
        return ResponseEntity.ok().body(saveSearchResponse);

    }
    @GetMapping("/save_searches/type/{type}")
    public ResponseEntity<List<SaveSearchResponseVM>> getSaveSearchByType(@PathVariable(name = "type") String type){
        log.info("get save search by type {}", type);
        if(!SaveSearchType.getListType().contains(type)){
            throw new FormValidateException("error","Type không nằm trong danh sách cho phép");
        }
        String login = SecurityUtils.getCurrentUserLogin().orElse(Constants.SYSTEM_ACCOUNT);
        List<SaveSearchResponseVM> saveSearchResponseVMS = Optional.ofNullable(searchRepository.findAllByCreatedByAndType(login,type))
            .orElseGet(ArrayList::new)
            .stream().filter(Objects::nonNull)
            .map(saveSearch->mapper.map(saveSearch,SaveSearchResponseVM.class))
            .collect(Collectors.toList());
        return ResponseEntity.ok().body(saveSearchResponseVMS);

    }
    @GetMapping("/save_searches/{id}")
    public ResponseEntity<SaveSearchResponseVM> getSaveSearchById(@PathVariable(name = "id") Long id)
        throws NotFoundException {
        Optional<SaveSearchDomain> optionalSaveSearchDomain = searchRepository.findById(id);
        if(optionalSaveSearchDomain.isEmpty()){
            throw new NotFoundException("Không tìm thấy đối tượng");
        }
        return ResponseEntity.ok().body(mapper.map(optionalSaveSearchDomain.get(),SaveSearchResponseVM.class));

    }
    @DeleteMapping("/save_searches/{id}")
    public ResponseEntity<?> updateSaveSearch(@PathVariable(name = "id") Long id){
    Optional<SaveSearchDomain> saveSearchOptional = searchRepository.findById(id);
        return saveSearchOptional.map(saveSearch -> {
            searchRepository.delete(saveSearch);
        return ResponseEntity.ok().build();
    }).orElseGet(() -> ResponseEntity.notFound().build());
    }

}
