package com.hungnt.web.rest;

import com.hungnt.config.Constants;
import com.hungnt.domain.CategoryDomain;
import com.hungnt.domain.product.ProductDomain;
import com.hungnt.repository.CategoryRepository;
import com.hungnt.security.AuthoritiesConstants;
import com.hungnt.service.product.ProductService;
import com.hungnt.utils.TextUtils;
import com.hungnt.web.rest.errors.FormValidateException;
import com.hungnt.web.rest.vm.category.CategoryRequestVM;
import com.hungnt.web.rest.vm.category.CategoryResponseVM;
import com.hungnt.web.rest.vm.common.UpdateStatusVM;
import com.hungnt.web.rest.vm.common.DataCollectionVM;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import ma.glasnost.orika.MapperFacade;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@Slf4j
public class CategoryResource {
    private final CategoryRepository categoryRepository;
    private final MapperFacade mapper;
    private final ProductService productService;
    public CategoryResource(CategoryRepository categoryRepository,
        MapperFacade mapper, ProductService productService) {
        this.categoryRepository = categoryRepository;
        this.mapper = mapper;
        this.productService = productService;
    }

    @GetMapping("/categories")
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN  + "\",\"" + AuthoritiesConstants.PRODUCT + "\")")
    public ResponseEntity<DataCollectionVM<CategoryResponseVM>> getCategories(String query,Pageable pageable){
        log.info("filter  category {}",query);
        Page<CategoryDomain> categoryDomains = categoryRepository.findAllByStatusIsNotLikeAndCategoryNameContainingOrCategoryCodeContaining("DELETED",query,query,pageable);
        val result = new DataCollectionVM<CategoryResponseVM>();
        result.setData(categoryDomains.map(m -> mapper.map(m, CategoryResponseVM.class)).getContent());
        result.setLimit(pageable.getPageSize());
        result.setPage(pageable.getPageNumber());
        result.setTotal(categoryDomains.getTotalElements());
        return ResponseEntity.ok().body(result);
    }

    @PostMapping("/categories")
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN  + "\",\"" + AuthoritiesConstants.PRODUCT +"\")")
    public ResponseEntity<CategoryResponseVM> createCategory(@RequestBody @Valid CategoryRequestVM categoryRequestVM){
      try {
          log.info("create category {}",categoryRequestVM);
          CategoryDomain categoryDomain = mapper.map(categoryRequestVM,CategoryDomain.class);
          categoryDomain.setCategoryCode(TextUtils.getCode("CT",categoryRepository.count()));
          return ResponseEntity.ok(mapper.map(categoryRepository.save(categoryDomain),CategoryResponseVM.class));
      }catch (Exception e){
          throw new FormValidateException("error",e.getMessage());
      }
    }
    @PutMapping("/categories/{id}")
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN  + "\",\"" + AuthoritiesConstants.PRODUCT +"\")")
    public ResponseEntity<CategoryResponseVM> updateCategory(@PathVariable("id") Long id, @RequestBody @Valid CategoryRequestVM categoryRequestVM){
        try {
            log.info("update category {}",categoryRequestVM);
            Optional<CategoryDomain> categoryDomainOptional = categoryRepository.findById(id);
            if(categoryDomainOptional.isEmpty()){
                throw new FormValidateException("error","not found");
            }
            CategoryDomain categoryDomain = categoryDomainOptional.get().update(categoryRequestVM);
            return ResponseEntity.ok(mapper.map(categoryRepository.save(categoryDomain),CategoryResponseVM.class));
        }catch (Exception e){
            throw new FormValidateException("error",e.getMessage());
        }
    }
    @PutMapping("/categories/update_status/{id}")
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN  + "\",\"" + AuthoritiesConstants.PRODUCT +"\")")
    public ResponseEntity<CategoryResponseVM> updateStatus(@PathVariable("id") Long id, @RequestBody @Valid UpdateStatusVM updateStatusVM){
        try {
            log.info("update category {}",updateStatusVM);
            Optional<CategoryDomain> categoryDomainOptional = categoryRepository.findById(id);
            if(categoryDomainOptional.isEmpty()){
                throw new FormValidateException("error","not found");
            }
            CategoryDomain categoryDomain = categoryDomainOptional.get();
            categoryDomain.setStatus(updateStatusVM.getStatus());
            categoryDomain=categoryRepository.save(categoryDomain);
            if(categoryDomain.getStatus().equals(Constants.STATUS_STOP_SALE)){
                List<ProductDomain> productDomainList = productService.findAllByCategoryId(categoryDomain.getId());
                if(!productDomainList.isEmpty()){
                    productDomainList.forEach(productDomain -> {
                        productService.updateStatusProduct(productDomain,updateStatusVM.getStatus());
                    });
                }
            }
            return ResponseEntity.ok(mapper.map(categoryDomain,CategoryResponseVM.class));
        }catch (Exception e){
            throw new FormValidateException("error",e.getMessage());
        }
    }
    @GetMapping("/categories/{id}")
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN  + "\",\"" + AuthoritiesConstants.PRODUCT +"\")")
    public ResponseEntity<CategoryResponseVM> getCategory(@PathVariable("id") Long id){
        try {
            log.info("get category {}",id);
            Optional<CategoryDomain> categoryDomainOptional = categoryRepository.findById(id);
            if(categoryDomainOptional.isEmpty()){
                throw new FormValidateException("error","not found");
            }
            CategoryDomain categoryDomain = categoryDomainOptional.get();
            return ResponseEntity.ok(mapper.map(categoryDomain,CategoryResponseVM.class));
        }catch (Exception e){
            throw new FormValidateException("error",e.getMessage());
        }
    }
    @DeleteMapping("/categories/{id}")
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN  + "\",\"" + AuthoritiesConstants.PRODUCT +"\")")
    public ResponseEntity<CategoryResponseVM> deleteCategory(@PathVariable("id") Long id){
//        try {
//            log.info("get category {}",id);
//            Optional<CategoryDomain> categoryDomainOptional = categoryRepository.findById(id);
//            if(categoryDomainOptional.isEmpty()){
//                throw new FormValidateException("error","not found");
//            }
//            CategoryDomain categoryDomain = categoryDomainOptional.get();
//            return ResponseEntity.ok(mapper.map(categoryDomain,CategoryResponseVM.class));
//        }catch (Exception e){
//            throw new FormValidateException("error",e.getMessage());
//        }
        return null;
    }

}
