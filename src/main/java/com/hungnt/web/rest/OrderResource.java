package com.hungnt.web.rest;

import com.hungnt.domain.User;
import com.hungnt.domain.customer.CustomerDomain;
import com.hungnt.domain.order.OrderDetailDomain;
import com.hungnt.domain.order.OrderDomain;
import com.hungnt.domain.product.ProductDetailDomain;
import com.hungnt.repository.UserRepository;
import com.hungnt.security.AuthoritiesConstants;
import com.hungnt.service.customer.CustomerService;
import com.hungnt.service.dto.OrderUpdateStatus;
import com.hungnt.service.order.OrderDetailService;
import com.hungnt.service.order.OrderService;
import com.hungnt.service.product.ProductDetailService;
import com.hungnt.web.rest.errors.FormValidateException;
import com.hungnt.web.rest.vm.common.DataCollectionVM;
import com.hungnt.web.rest.vm.customer.CustomerResponseVM;
import com.hungnt.web.rest.vm.filter.OrderFilter;
import com.hungnt.web.rest.vm.order.OrderDetailResponseVM;
import com.hungnt.web.rest.vm.order.OrderResponseVM;
import com.hungnt.web.rest.vm.order.OrderVM;
import com.hungnt.web.rest.vm.product.ProductDetailResponseVM;
import com.hungnt.web.rest.vm.user.UserResponseNotAuthorizes;
import com.hungnt.web.rest.vm.user.UserResponseVM;
import java.util.List;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import ma.glasnost.orika.MapperFacade;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@Slf4j
public class OrderResource {

    private final OrderService orderService;
    private final CustomerService customerService;
    private final UserRepository userRepository;
    private final OrderDetailService orderDetailService;
    private final ProductDetailService productDetailService;
    private final MapperFacade mapper;

    public OrderResource(OrderService orderService,
        CustomerService customerService, UserRepository userRepository,
        OrderDetailService orderDetailService,
        ProductDetailService productDetailService, MapperFacade mapper) {
        this.orderService = orderService;
        this.customerService = customerService;
        this.userRepository = userRepository;
        this.orderDetailService = orderDetailService;
        this.productDetailService = productDetailService;
        this.mapper = mapper;
    }

    @PostMapping("/orders")
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN  + "\",\"" + AuthoritiesConstants.ORDER +"\")")
    public ResponseEntity<OrderResponseVM> create(@RequestBody @Valid OrderVM orderVM) {
        try {
            log.info("create order {}", orderVM.toString());
            OrderDomain orderDomain = orderService.createOrder(orderVM);
            log.info("order {}", orderDomain.toString());
            OrderResponseVM orderResponseVM = mapper.map(orderDomain, OrderResponseVM.class);
            CustomerDomain customerDomain = customerService.findById(orderDomain.getCustomerId());
            orderResponseVM.setCustomer(mapper.map(customerDomain, CustomerResponseVM.class));
            return ResponseEntity.ok(orderResponseVM);
        } catch (Exception e) {
            throw new FormValidateException("error", e.getMessage());
        }
    }

    @GetMapping("/orders")
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN  + "\",\"" + AuthoritiesConstants.ORDER +"\")")
    public ResponseEntity<DataCollectionVM<OrderResponseVM>> getOrders(OrderFilter filter,
        Pageable pageable) {
        log.info("Request To Filter And Search product {}", filter.toString());
        Page<OrderDomain> orderDomains = orderService.findByCriteria(filter, pageable);
        List<OrderResponseVM> orderResponseVMS = orderDomains
            .map(m -> mapper.map(m, OrderResponseVM.class)).getContent();
        if (!orderResponseVMS.isEmpty()) {
            orderResponseVMS.forEach(orderResponseVM -> {
                if (orderResponseVM.getCustomerId() != null) {
                    CustomerDomain customerDomain = customerService
                        .findById(orderResponseVM.getCustomerId());
                    orderResponseVM
                        .setCustomer(mapper.map(customerDomain, CustomerResponseVM.class));
                }

            });
        }
        val result = new DataCollectionVM<OrderResponseVM>();
        result.setData(orderResponseVMS);
        result.setLimit(pageable.getPageSize());
        result.setPage(pageable.getPageNumber());
        result.setTotal(orderDomains.getTotalElements());
        return ResponseEntity.ok().body(result);
    }

    @PutMapping("/orders/status/{id}")
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN  + "\",\"" + AuthoritiesConstants.ORDER +"\")")
    public ResponseEntity<OrderResponseVM> updateStatus(@PathVariable("id") Long id,
        @RequestBody @Valid
            OrderUpdateStatus orderUpdateStatus) {
        try {
            log.info("update order {}", orderUpdateStatus.toString());
            OrderDomain orderDomain = orderService.updateStatus(orderUpdateStatus);
            return ResponseEntity.ok(mapper.map(orderDomain, OrderResponseVM.class));
        } catch (Exception e) {
            throw new FormValidateException("error", e.getMessage());
        }
    }

    @GetMapping("/orders/code/{code}")
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN  + "\",\"" + AuthoritiesConstants.ORDER +"\")")
    public ResponseEntity<OrderResponseVM> getOrder(@PathVariable("code") String code) {
        try {
            log.info("get order {}", code);
            OrderDomain orderDomain = orderService.findByCode(code);
            CustomerDomain customerDomain = customerService.findById(orderDomain.getCustomerId());
            User user = userRepository.findById(orderDomain.getUserId()).get();
            OrderResponseVM orderResponseVM = mapper.map(orderDomain, OrderResponseVM.class);
            orderResponseVM.setCustomer(mapper.map(customerDomain, CustomerResponseVM.class));
            orderResponseVM.setUser(mapper.map(user, UserResponseNotAuthorizes.class));
            return ResponseEntity.ok(orderResponseVM);
        } catch (Exception e) {
            throw new FormValidateException("error", e.getMessage());
        }
    }

    @GetMapping("/orders/details/{orderId}")
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN  + "\",\"" + AuthoritiesConstants.ORDER +"\")")
    public ResponseEntity<List<OrderDetailResponseVM>> getOrderDetails(
        @PathVariable("orderId") Long id) {
        try {
            log.info("get orderDetail by orderId {}", id);
            List<OrderDetailDomain> orderDetailDomains = orderDetailService
                .getOrderDetailByOrderId(id);
            List<OrderDetailResponseVM> orderDetailResponseVMS = mapper
                .mapAsList(orderDetailDomains, OrderDetailResponseVM.class);
            orderDetailResponseVMS.forEach(item->{
                ProductDetailDomain productDetailDomain = productDetailService.finById(item.getProductDetailId());
                item.setProductDetail(mapper.map(productDetailDomain, ProductDetailResponseVM.class));
            });
            return ResponseEntity.ok(orderDetailResponseVMS);
        } catch (Exception e) {
            throw new FormValidateException("error", e.getMessage());
        }
    }
}
