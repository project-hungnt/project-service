package com.hungnt.web.rest;

import com.hungnt.domain.customer.CustomerDomain;
import com.hungnt.repository.customer.CustomerRepository;
import com.hungnt.security.AuthoritiesConstants;
import com.hungnt.service.customer.CustomerService;
import com.hungnt.utils.TextUtils;
import com.hungnt.web.rest.errors.FormValidateException;
import com.hungnt.web.rest.vm.common.DataCollectionVM;
import com.hungnt.web.rest.vm.customer.CustomerRequestVM;
import com.hungnt.web.rest.vm.customer.CustomerResponseVM;
import com.hungnt.web.rest.vm.filter.CustomerFilter;
import java.util.Optional;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import ma.glasnost.orika.MapperFacade;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@Slf4j
public class CustomerResource {

    private final CustomerService customerService;
    private final CustomerRepository customerRepository;
    private final MapperFacade mapper;

    public CustomerResource(CustomerService customerService,
        CustomerRepository customerRepository, MapperFacade mapper) {
        this.customerService = customerService;
        this.customerRepository = customerRepository;
        this.mapper = mapper;
    }

    @GetMapping("/customers")
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN  + "\",\"" + AuthoritiesConstants.CUSTOMER +"\")")
    public ResponseEntity<DataCollectionVM<CustomerResponseVM>> getCustomers(CustomerFilter filter,
        Pageable pageable) {
        log.info("Request To Filter And Search customer {}", filter.toString());
        Page<CustomerDomain> customerDomains = customerService.findByCriteria(filter, pageable);
        val result = new DataCollectionVM<CustomerResponseVM>();
        result.setData(
            customerDomains.map(m -> mapper.map(m, CustomerResponseVM.class)).getContent());
        result.setLimit(pageable.getPageSize());
        result.setPage(pageable.getPageNumber());
        result.setTotal(customerDomains.getTotalElements());
        return ResponseEntity.ok().body(result);
    }

    @PostMapping("/customers")
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN  + "\",\"" + AuthoritiesConstants.CUSTOMER +"\")")
    public ResponseEntity<CustomerResponseVM> createCustomer(
        @RequestBody @Valid CustomerRequestVM customerRequestVM) {
        try {
            log.info("create customer {}", customerRequestVM);
            Optional<CustomerDomain> customerDomainOptional = customerRepository.findByCode(customerRequestVM.getCode());
            if (customerDomainOptional.isPresent()) {
                throw new FormValidateException("", "Đã tồn tại mã khách hàng này");
            }
            CustomerDomain customerDomain = mapper.map(customerRequestVM, CustomerDomain.class);
            customerDomain.setCode(TextUtils.getCode("C", customerRepository.count()));
            return ResponseEntity.ok(mapper.map(customerRepository.save(customerDomain), CustomerResponseVM.class));
        } catch (Exception e) {
            throw new FormValidateException("error", e.getMessage());
        }
    }

    @PutMapping("customers/{id}")
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN  + "\",\"" + AuthoritiesConstants.CUSTOMER +"\")")
    public ResponseEntity<CustomerResponseVM> updateCustomer(@PathVariable("id") Long id,
        @RequestBody @Valid CustomerRequestVM customerRequestVM) {
        try {
            log.info("update customer {}", customerRequestVM);
            Optional<CustomerDomain> customerDomainOptional = customerRepository
                .findById(id);
            if (customerDomainOptional.isEmpty()) {
                throw new FormValidateException("error", "Không tìm thấy khách hàng");
            }
            CustomerDomain customerDomain = customerDomainOptional.get().update(customerRequestVM);
            log.info("create customer {}", customerRequestVM);
            return ResponseEntity
                .ok(mapper.map(customerService.save(customerDomain), CustomerResponseVM.class));
        } catch (Exception e) {
            throw new FormValidateException("error", e.getMessage());
        }
    }

    @GetMapping("/customers/{id}")
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN  + "\",\"" + AuthoritiesConstants.CUSTOMER +"\")")
    public ResponseEntity<CustomerResponseVM> getCustomer(@PathVariable("id") Long id) {
        log.info("get customer {}", id);
        Optional<CustomerDomain> customerDomainOptional = customerRepository
            .findById(id);
        if (customerDomainOptional.isEmpty()) {
            throw new FormValidateException("error", "Không tìm thấy khách hàng");
        }
        return ResponseEntity
            .ok(mapper.map(customerDomainOptional.get(), CustomerResponseVM.class));
    }

    @GetMapping("/customers/code/{code}")
    @PreAuthorize("hasAnyRole(\"" + AuthoritiesConstants.ADMIN  + "\",\"" + AuthoritiesConstants.CUSTOMER +"\")")
    public ResponseEntity<CustomerResponseVM> getCustomerByCode(@PathVariable("code") String code) {
        log.info("get customer {}", code);
        Optional<CustomerDomain> customerDomainOptional = customerRepository.findByCode(code);
        if (customerDomainOptional.isEmpty()) {
            throw new FormValidateException("error", "Không tìm thấy khách hàng");
        }
        return ResponseEntity
            .ok(mapper.map(customerDomainOptional.get(), CustomerResponseVM.class));
    }
}
