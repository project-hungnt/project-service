package com.hungnt.web.rest;

import com.hungnt.domain.qr.QrDomain;
import com.hungnt.repository.qr.QrRepository;
import com.hungnt.service.qr.QrService;
import com.hungnt.web.rest.vm.qr.CreateQrVM;
import com.hungnt.web.rest.vm.qr.QrVmResponse;
import javax.validation.Valid;
import lombok.val;
import ma.glasnost.orika.MapperFacade;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class QrResource {
    private final QrRepository qrRepository;
    private final MapperFacade mapper;
    private final QrService qrService;

    public QrResource(QrRepository qrRepository, MapperFacade mapper,
        QrService qrService) {
        this.qrRepository = qrRepository;
        this.mapper = mapper;
        this.qrService = qrService;
    }

    @PostMapping("/qr")
    public ResponseEntity<QrVmResponse> getQr(@RequestBody @Valid CreateQrVM createQrVM){
        QrDomain domain = mapper.map(createQrVM,QrDomain.class);
        val qrDomainOptional = qrRepository.findByReferenceId(createQrVM.getReferenceId());
        if(qrDomainOptional.isPresent()){
            domain=qrDomainOptional.get().update(createQrVM);
        }
        return ResponseEntity.ok(mapper.map(qrService.generateQr(domain),QrVmResponse.class));
    }
}
