package com.hungnt.repository;

import com.hungnt.domain.CategoryDomain;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CategoryRepository extends JpaRepository<CategoryDomain,Long> {
    Page<CategoryDomain> findAllByStatusIsNotLikeAndCategoryNameContainingOrCategoryCodeContaining(String name,String code,String status, Pageable pageable);
}
