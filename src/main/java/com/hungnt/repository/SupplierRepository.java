package com.hungnt.repository;

import com.hungnt.domain.supplier.SupplierDomain;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface SupplierRepository extends JpaRepository<SupplierDomain, Long>,JpaSpecificationExecutor<SupplierDomain> {
    Optional<SupplierDomain> findByCode(String code);
}
