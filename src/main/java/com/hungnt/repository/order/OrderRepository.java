package com.hungnt.repository.order;

import com.hungnt.domain.order.OrderDomain;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<OrderDomain, Long>,JpaSpecificationExecutor<OrderDomain> {

    Optional<OrderDomain> findByCode(String code);
}
