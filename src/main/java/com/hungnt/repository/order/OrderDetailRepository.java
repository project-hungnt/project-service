package com.hungnt.repository.order;

import com.hungnt.domain.order.OrderDetailDomain;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderDetailRepository extends JpaRepository<OrderDetailDomain, Long>,JpaSpecificationExecutor<OrderDetailDomain>{
    List<OrderDetailDomain> findAllByOrOrderId(Long id);
}
