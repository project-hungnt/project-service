package com.hungnt.repository.purchase;

import com.hungnt.domain.purchase.PurchaseOrderDetailDomain;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface PurchaseOrderDetailRepository extends JpaRepository<PurchaseOrderDetailDomain, Long>,JpaSpecificationExecutor<PurchaseOrderDetailDomain>{
    List<PurchaseOrderDetailDomain> findAllByPurchaseOrderId(Long id);
}
