package com.hungnt.repository.purchase;

import com.hungnt.domain.order.OrderDomain;
import com.hungnt.domain.purchase.PurchaseOrdersDomain;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface PurchaseOrderRepository extends JpaRepository<PurchaseOrdersDomain, Long>,
    JpaSpecificationExecutor<PurchaseOrdersDomain> {

    Optional<PurchaseOrdersDomain> findByCode(String code);
}
