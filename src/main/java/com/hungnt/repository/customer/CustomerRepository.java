package com.hungnt.repository.customer;

import com.hungnt.domain.customer.CustomerDomain;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<CustomerDomain, Long>,JpaSpecificationExecutor<CustomerDomain> {

    Optional<CustomerDomain> findByCode(String code);
}
