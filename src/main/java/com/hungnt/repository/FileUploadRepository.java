package com.hungnt.repository;

import com.hungnt.domain.FileUpload;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileUploadRepository extends JpaRepository<FileUpload ,Long> {

    List<FileUpload> findAllByProviderAndReferenceId(String type,Long id);
}
