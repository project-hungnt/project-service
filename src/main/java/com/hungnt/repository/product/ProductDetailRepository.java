package com.hungnt.repository.product;

import com.hungnt.domain.product.ProductDetailDomain;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductDetailRepository extends JpaRepository<ProductDetailDomain, Long>,JpaSpecificationExecutor<ProductDetailDomain> {
    List<ProductDetailDomain> findAllByProductId(Long productId);
}
