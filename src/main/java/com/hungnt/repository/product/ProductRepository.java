package com.hungnt.repository.product;

import com.hungnt.domain.product.ProductDomain;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<ProductDomain, Long>,JpaSpecificationExecutor<ProductDomain> {

    Optional<ProductDomain> findByProductCode(String productCode);
    List<ProductDomain> findAllByCategoryId(Long id);
}
