package com.hungnt.repository.qr;

import com.hungnt.domain.qr.QrDomain;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QrRepository extends JpaRepository<QrDomain,Long> {
    Optional<QrDomain> findByReferenceId(Long referenceId);
}
