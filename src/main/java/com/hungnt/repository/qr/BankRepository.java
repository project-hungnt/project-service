package com.hungnt.repository.qr;

import com.hungnt.domain.qr.BankDomain;
import com.hungnt.domain.qr.QrDomain;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface BankRepository extends JpaRepository<BankDomain, Long>,JpaSpecificationExecutor<BankDomain> {
        Optional<BankDomain> findByCode(String code);
}
