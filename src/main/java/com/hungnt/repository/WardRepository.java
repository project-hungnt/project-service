package com.hungnt.repository;

import com.hungnt.domain.WardDomain;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WardRepository extends JpaRepository<WardDomain, Long> {

    List<WardDomain> findByCityCodeAndDistrictCode(String cityCode, String districtCode);

    Optional<WardDomain> findByCityCodeAndDistrictCodeAndWardCode(String cityCode, String districtCode, String wardName);

    boolean existsByCityCodeAndDistrictCodeAndWardCode(String cityCode, String districtCode, String wardCode);
}
