package com.hungnt.repository;

import com.hungnt.domain.LocationDomain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationRepository extends JpaRepository<LocationDomain,Long> {

}
