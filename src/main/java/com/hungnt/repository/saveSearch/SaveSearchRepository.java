package com.hungnt.repository.saveSearch;

import com.hungnt.domain.SaveSearchDomain;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SaveSearchRepository extends JpaRepository<SaveSearchDomain,Long> {
    List<SaveSearchDomain> findAllByCreatedByAndType(String login,String type);
}
