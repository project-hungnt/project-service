package com.hungnt.repository;

import com.hungnt.domain.CityDomain;
import com.hungnt.domain.qr.BankDomain;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface CityRepository extends JpaRepository<CityDomain, Long>,JpaSpecificationExecutor<CityDomain> {
    List<CityDomain> findByCountryCode(String countryCode);

    Optional<CityDomain> findTopByCityCode(String cityCode);

    boolean existsByCityCode(String cityCode);


}
