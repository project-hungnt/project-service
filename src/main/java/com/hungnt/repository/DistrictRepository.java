package com.hungnt.repository;

import com.hungnt.domain.DistrictDomain;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface DistrictRepository extends JpaRepository<DistrictDomain, Long>,JpaSpecificationExecutor<DistrictDomain> {

    List<DistrictDomain> findByCityCode(String cityCode);

    Optional<DistrictDomain> findTopByCityCodeAndDistrictCode(String cityCode, String districtCode);

    boolean existsByDistrictCodeAndCityCode(String districtCode, String cityCode);

}
