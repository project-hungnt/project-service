package com.hungnt.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String USER = "ROLE_USER";

    public static final String STAFF = "ROLE_STAFF";
    public static final String ORDER = "ROLE_ORDER";
    public static final String PRODUCT = "ROLE_PRODUCT";
    public static final String CUSTOMER = "ROLE_CUSTOMER";
    public static final String SUPPLIER = "ROLE_SUPPLIER";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    private AuthoritiesConstants() {
    }
}
