package com.hungnt.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SaveSearchType {
    private static String CUSTOMER ="customer";
    private static String CUSTOMER_SETTING_COLUMN ="setting_columns_customer";
    private static String PRODUCT ="product";
    private static String PRODUCT_SETTING_COLUMN ="setting_columns_product";
    private static String ORDER ="order";
    private static String ORDER_SETTING_COLUMN ="setting_columns_order";
    private static String USER ="user";
    private static String CATEGORY ="category";
    private static String SUPPLIER ="supplier";
    private static String INVENTORY ="inventory";
    private static String INVENTORY_SETTING_COLUMN ="setting_columns_inventory";
    private static String SUPPLIER_SETTING_COLUMN ="setting_columns_supplier";

    public static List<String> getListType(){
        return Arrays.asList(CUSTOMER,CUSTOMER_SETTING_COLUMN,SUPPLIER,SUPPLIER_SETTING_COLUMN,USER,CATEGORY,INVENTORY,INVENTORY_SETTING_COLUMN, PRODUCT_SETTING_COLUMN,PRODUCT,ORDER,ORDER_SETTING_COLUMN);
    }
}
